==================
Reading Arepo data
==================

This module reads the output of Arepo simulations. 

---------------------------------------------------------------------------------------------------
Basics
---------------------------------------------------------------------------------------------------

The routines are not be specific to any particular simulation, though some defaults are chosen 
with the Illustris simulation suite in mind. The following code creates an object representing an
Arepo simulation::

	from hydrotools.arepo import arepo
	
	ArepoSim = arepo.ArepoSimulation('illustris_tng75', 'planck15', path = 'path/to/sim/data',
		cache_dir = 'path/to/cache', physical_units = True, verbose = False, paranoid = True)

The given cosmology must correspond to a valid set of cosmological parameters in the colossus
toolkit. Furthermore, the ArepoSimulation object must be given a list of the redshifts of the 
outputs (this information could, in principle, be collected, but this behavior is currently not
implemented). This list must be in ``hydrotools/data/snaps_<sim_name>.txt``, e.g. 
``hydrotools/data/snaps_illustris_tng75.txt`` in this example. We can now get an object for a 
given snapshot::

	snap_idx = ArepoSim.n_snaps - 1
	Snapshot = ArepoSim.getSnapshot(snap_idx)

In this case, we have chosen the last snapshot (probably :math:`z = 0`). The snapshot object
contains the most important data loading functions. For example, let's load some properties for
all subhalos (i.e., galaxies) above a certain stellar mass::

	# First, load the stellar masses of all subhalos
	sh_data = Snapshot.getSubhaloData(fields = ['SubhaloMassType'])
	star_mass = sh_data['SubhaloMassType'][:, 4]
	
	# Only keep the indices of those that have stellar masses above 1E11 and load a larger list 
	# of fields for those subhalos
	sh_idxs = np.where(star_mass >= 1E11)
	sh_data = Snapshot.getSubhaloData(idxs = sh_idxs, 
		fields = ['SubhaloGrNr', 'SubhaloMassType', 'SubhaloSFR'])
	
Many more fields are available, depending on the simulation. Now let's also load some halo 
properties by using the group indices::

	grp_data = Snapshot.getGroupData(idxs = sh_data['SubhaloGrNr'], 
		fields = ['GroupNsubs', 'GroupFirstSub', 'Group_M_Crit200'])

In this module, particle data is loaded on a per-subhalo or per-group basis. For example, the 
following code would load the masses and coordinates of all stellar particles in the 
FOF group with index 10::
	
	data_stars = Snapshot.getGroupParticles(10, ptl_type = 4, fields = ['Coordinates', 'Masses'])

or similarly for the gas particles in galaxy with index 20::

	data_gas = Snapshot.getSubhaloParticles(20, ptl_type = 0, 
		fields = ['Coordinates', 'Masses', 'GFM_Metallicity', 'InternalEnergy', 'StarFormationRate'])

Any other property in the particle catalogs can be loaded as well. Moreover, the user can specify 
a sub-field of a multi-dimensional array. This can be useful to avoid loading very large datasets.
For example, the ``GFM_Metals`` field has 10 entries, but we can load only the fraction of hydrogen
by specifying sub-field 0::

	f_hydrogen = Snapshot.getSubhaloParticles(20, ptl_type = 0, fields = ['GFM_Metals::0'])
	
The characters after ``::`` must be an integer, and must be a valid index into the 2-dimensional
array in the given field. Note that this behavior is currently implemented only for particles but
not for galaxy/halo data.

---------------------------------------------------------------------------------------------------
Unit conversion
---------------------------------------------------------------------------------------------------

Note that the user needs to pass the internal units system as it is not listed in the hdf5 
files. By default, a unit system corresponding to the following settings is chosen:

.. table::
	:widths: auto

	=================================================== ===============
	Unit                                                Value
	=================================================== ===============
	``UnitLength_in_cm``                                3.08568e+21
	``UnitMass_in_g``                                   1.989e+43
	``UnitVelocity_in_cm_per_s``                        100000
	=================================================== ===============

If the physical_units parameter is set (default = True), all routines in this module return
the properties of halos, subhalos, and particles in a different, more physical unit system (see
:doc:`units`). Note, however, that not all fields may be listed in the 
:func:`~hydrotools.arepo.arepo.ArepoSnapshot.physicalUnits` function, in which case they are returned in the units used 
in the hdf5 files.

---------------------------------------------------------------------------------------------------
Module reference
---------------------------------------------------------------------------------------------------

.. automodule:: hydrotools.arepo.arepo
    :members:
