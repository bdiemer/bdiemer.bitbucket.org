===========================
Output format (group data)
===========================

This document explains the data that can be extracted from the Illustris simulations using 
the :func:`~hydrotools.interface.interface.extractGroupData` function. Note that the output from this 
interface can also be achieved using the :func:`~hydrotools.interface.interface.extractGalaxyData` interface; 
however, if you are
interested in the properties of groups (or halos) as opposed to galaxies (or subhalos), this 
interface is more elegant. 

---------------------------
Fundamental types of output
---------------------------

Group output from hydrotools is grouped into certain categories. Note that these are generally
a subset of the richer interface for galaxies:

.. table::
   :widths: auto

   ================ =======================================
   Output type      Explanation
   ================ =======================================
   ``catgrp``       Fields from the FOF group (halo) catalog
   ``ptlgas``       Properties of gas particles (type 0)
   ``ptldm``        Properties of dark matter particles (type 1)
   ``ptlstr``       Properties of stellar particles (type 4, excluding wind)
   ``profile``      Spherically averaged profiles
   ================ =======================================

These identifier are the prefixes to all fields in the output file (see below). They also 
organize the input to the script that extracts the data (see :doc:`interface_interface`).
In SUBFIND catalogs, "groups" refer to friends-of-friends groups, and subhalos to their bound 
sub-groups. In this interface, only group-level properties can be extracted.

---------------------------
Datasets in the output file
---------------------------

The hydrotools script returns a single .hdf5 file. This file contains two types of output: datasets
that all share as their first dimension the number of groups output, ``n_groups``, and a 
``config`` group which contains attributes related to the configuration and other helpful 
information. The output quantities depend on the parameters to the 
:func:`~hydrotools.interface.interface.extractGroupData` function. In the table below, the dimensions 
refer to some of the following numbers:

* ``n_ptlgas``: The total number of gas particles extracted
* ``n_ptldm``: The total number of dark matter particles extracted
* ``n_ptlstr``: The total number of stellar particles extracted
* ``n_prof_bin``: The number of bins in radial profiles

Some fields below such as ``catgrp_GroupMassType`` have dimensionality 6, corresponding to the 
six particle types in Gadget / Arepo. The types used in hydrotools are:

* ``0``: gas
* ``1``: dark matter
* ``4``: stellar particles
* ``5``: black holes

The table below represents an incomplete lists of datasets that can appear in the output file. 
There are many variations on these outputs that follow similar patterns:

.. table::
   :widths: auto

   ========================================== =================== =============== ======================================
   Field                                      Dimensions          Units           Explanation
   ========================================== =================== =============== ======================================
   catgrp_id                                  --                  --              Group ID 
   catgrp_is_primary                          --                  --              Is this the primary subhalo (central) or a satellite
   catgrp_GroupPos                            3                   ckpc/h          Position of group in box (most bound particle)
   catgrp_GroupCM                             3                   ckpc/h          Position of group in box (center of mass)
   catgrp_GroupVel                            3                   km/s            Peculiar velocity of group
   catgrp_GroupMassType                       6                   Msun            Total mass in gas/DM/stars/BHs       
   catgrp_Group_M_Crit500                     --                  Msun            Total mass within R500c
   catgrp_Group_M_Crit200                     --                  Msun            Total mass within R200c
   catgrp_Group_M_TopHat200                   --                  Msun            Total mass within Rvir
   catgrp_Group_M_Mean200                     --                  Msun            Total mass within R200m
   catgrp_Group_R_Crit500                     --                  kpc             R500c
   catgrp_Group_R_Crit200                     --                  kpc             R200c
   catgrp_Group_R_TopHat200                   --                  kpc             Rvir 
   catgrp_Group_R_Mean200                     --                  kpc             R200m
   catgrp_GroupNsubs                          --                  --              The number of subhalos in this SH’s group
   catgrp_GroupBHMdot                         --                  Msun/yr         Summed mass accretion rate of black holes in group
   catgrp_GroupSFR                            --                  Msun/yr         Summed SFR in group
   catgrp_GroupGasMetallicity                 --                  --              Mass-weighted gas metallicity
   catgrp_GroupStarMetallicity                --                  --              Mass-weighted stellar metallicity
   ptlgas_n                                   --                  --              Number of gas particles bound to this subhalo
   ptlgas_first                               --                  --              Index of first particle in this SH into the ptlgas array
   ptlgas_*                                   n_ptlgas            ?               Properties of gas particles, names as in Arepo files
   ptldm_n                                    --                  --              Number of dark matter particles bound to this subhalo
   ptldm_first                                --                  --              Index of first particle in this SH into the ptldm array
   ptldm_*                                    n_ptldm             ?               Properties of dark matter particles, names as in Arepo files
   ptlstr_n                                   --                  --              Number of stellar particles bound to this subhalo
   ptlstr_first                               --                  --              Index of first particle in this SH into the ptlstr array
   ptlstr_*                                   n_ptlstr            ?               Properties of stellar particles, names as in Arepo files
   profile_bins                               n_prof_bin          kpc             The outer edges of the profile bins in kpc
   profile_bins_area                          n_prof_bin          kpc^2           The area of each profile bin; multiply with 2d profiles to get total
   profile_bins_volume                        n_prof_bin          kpc^3           The volume of each profile bin; multiply with 3d profiles to get total
   profile_gas_rho_3d                         n_prof_bin          Msun/kpc^3      Total gas volume density profile
   profile_rho_neutral_H_3d                   n_prof_bin          Msun/kpc^3      Neutral hydrogen density profile
   profile_f_neutral_H_3d                     n_prof_bin          --              Neutral hydrogen fraction profile
   profile_gas_e_int_3d                       n_prof_bin          km^2/s^2        Mass-weighted average internal energy per unit mass
   profile_gas_f_electron_3d                  n_prof_bin          --              Mass-weighted average electron abundance per hydrogen abundance (where X_H = 0.76)
   profile_gas_pressure_3d                    n_prof_bin          g/cm/s^2        Mass-weighted average thermal pressure
   profile_gas_temperature_3d                 n_prof_bin          K               Mass-weighted average temperature (not physical for star-forming cells)
   profile_gas_metallicity_3d                 n_prof_bin          --              Mass-weighted average metallicity in 2D or 3D bins
   profile_sfr_3d                             n_prof_bin          Msun/yr/kpc^3   SFR density profile
   profile_dm_rho_3d                          n_prof_bin          Msun/kpc^3      Total dark matter volume density profile
   profile_star_rho_3d                        n_prof_bin          Msun/kpc^3      Total stellar volume density profile
   profile_star_rho_insitu_3d                 n_prof_bin          Msun/kpc^3      Density profile of in-situ formed stars
   profile_star_rho_exsitu_3d                 n_prof_bin          Msun/kpc^3      Density profile of ex-situ formed stars
   profile_star_age_3d                        n_prof_bin          --              Mass-weighted stellar age
   profile_star_metallicity_3d                n_prof_bin          --              Mass-weighted average stellar metallicity
   profile_star_metal_H_3d                    n_prof_bin          --              Mass-weighted average hyrodgen fraction in stars. Only available in TNG.
   profile_star_metal_He_3d                   n_prof_bin          --              Mass-weighted average helium fraction in stars. Only available in TNG.
   profile_star_metal_C_3d                    n_prof_bin          --              Mass-weighted average carbon fraction in stars. Only available in TNG.
   profile_star_metal_N_3d                    n_prof_bin          --              Mass-weighted average nitrogen fraction in stars. Only available in TNG.
   profile_star_metal_O_3d                    n_prof_bin          --              Mass-weighted average oxygen fraction in stars. Only available in TNG.
   profile_star_metal_Ne_3d                   n_prof_bin          --              Mass-weighted average neon fraction in stars. Only available in TNG.
   profile_star_metal_Mg_3d                   n_prof_bin          --              Mass-weighted average magnesium fraction in stars. Only available in TNG.
   profile_star_metal_Si_3d                   n_prof_bin          --              Mass-weighted average silicon fraction in stars. Only available in TNG.
   profile_star_metal_Fe_3d                   n_prof_bin          --              Mass-weighted average iron fraction in stars. Only available in TNG.
   profile_star_metal_other_3d                n_prof_bin          --              Mass-weighted average other elements fraction in stars. Only available in TNG.
   ========================================== =================== =============== ======================================

-------------------
The config group
-------------------

The ``config`` group contains (as attributes) the parameters to the 
:func:`~hydrotools.interface.interface.extractGroupData` function. Note that many of those parameters may not
be used in a given run.
