-----------------------
Adding an output type
-----------------------

The output of hydrotools is grouped into types of output such as catalog properties, profiles,
scalars, maps and so on. Within each output type, there are a number of fields, for example
profiles of stellar density, gas temperature and so on. This document describes how to add a 
new output type. For adding a field to an existing type, see :doc:`developers_adding_field`.
To add a new type of output to hydrotools, follow these steps:

* Add an identifier of the new type to the ``output_types`` list in ``hydrotools/common.py``. This
  tag should be as short as possible and clearly identify the nature of the new output type.
* In ``hydrotools/illustris/illustris_common.py``, add a new default field list, e.g.::

    default_morphology_fields = ['kappa']
    """
    By default, these morphology fields are output.
    """

* Still in ``hydrotools/illustris/illustris_common.py``, add a dictionary for the properties of 
  all possible fields in your new output type, e.g.::

    morphology_props = {}
    morphology_props['allow_arbitrary'] = False
    morphology_props['kappa']     = {'dependencies': []}
  
  The ``dependencies`` dictionary entry indicates all other fields your computation depends on. 
  It is critical that this list be complete; if fields are missing, the user may receive 
  somewhat cryptic error messages because the missing quantities could not be found.
* In ``interface/interface_*.py`` (depending on the interface(s) for which this output type is
  relevant), add two timers (preparation and computation) that carry the same name as your output
  type, e.g. ``prep_morphology`` and ``morphology``.
* In ``interface/interface.py``, add a parameter ``<output_type>_get`` (which should probably
  be set to ``False`` by default) and a ``<output_type>_fields`` parameter that indicates which
  fields are output (which you can set to the default field list defined earlier), e.g.::

    morphology_get = False, morphology_fields = ils_com.default_morphology_fields

* Now it is time to integrate the new calculation into the workflow in the 
  ``illustris/illustris_work_*.py`` file(s), depending on the interface for which the output
  type is active. For example, for galaxies, we add::
  
    if gd['morphology_do']:
        common.stopStartTimer(gd, 'morphology')
        common.printOutput(gd, 'Loading morphology...')
        illustris_cat.loadMorphology(gd, queue_out)

  in the ``globalWork`` function of ``illustris/illustris_work_galaxies.py``. In this case, the
  function is not parallelized over galaxies but only executed once. If we wish to execute the 
  function for each galaxy separately, we add something like::
  
    if gd['map_do']:
        common.stopStartTimer(gd, 'map')
        illustris_map.computeMaps(gd, dd, queue_out, i, 0)

  in the ``galaxyWork`` routine. Note the difference in the function interface: in addition to
  the global dictionary ``gd`` and the output queue, the function now receives the galaxy
  dictionary ``dd``, the index of the galaxy, and the process number. The interface is 
  identical for other extraction routines.
* Impelement the function(s) called above. It is probably best to add a new file
  ``illustris/illustris_<output_type>.py`` for the new code. Note that the list of fields that
  needs to be computed is ``gd['<output_type>_compute']`` whereas 
  ``gd['<output_type>_fields']`` gives the list of fields that are output. 
* Document the new output type in ``docs/source/output_<interface>.py``. 

In all of the above steps, please maintain the order of output types given in ``common.py`` to 
avoid confusion.
