��>,      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Installation�h]�h	�Text����Installation�����}�(�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�G/Users/benedito/University/code/hydrotools/docs/source/installation.rst�hKubh	�	paragraph���)��}�(hX  Hydrotools must be run on a machine where the simulation data is available on disk (not via the
online database). The code is, in principle, compatible with both Python 2.7 and Python 3.x.
However, the code is developed and tested in Python 3, which is thus the recommended version.�h]�hX  Hydrotools must be run on a machine where the simulation data is available on disk (not via the
online database). The code is, in principle, compatible with both Python 2.7 and Python 3.x.
However, the code is developed and tested in Python 3, which is thus the recommended version.�����}�(hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh.)��}�(hX�  You can clone hydrotools
directly from the public BitBucket repository, but you will almost certainly want to change at
least a few lines of the code. Thus, the recommended workflow is as follows. First, on the
BitBucket website, fork the repository (https://bitbucket.org/bdiemer/hydrotools); you
should end up with a repository called https://bitbucket.org/yourname/hydrotools (where yourname
represents your BitBucket/Atlassian username). Now, clone that repository onto your local
machine:�h]�(h��You can clone hydrotools
directly from the public BitBucket repository, but you will almost certainly want to change at
least a few lines of the code. Thus, the recommended workflow is as follows. First, on the
BitBucket website, fork the repository (�����}�(hh=hhhNhNubh	�	reference���)��}�(h�(https://bitbucket.org/bdiemer/hydrotools�h]�h�(https://bitbucket.org/bdiemer/hydrotools�����}�(hhGhhhNhNubah}�(h!]�h#]�h%]�h']�h)]��refuri�hIuh+hEhh=ubh�.); you
should end up with a repository called �����}�(hh=hhhNhNubhF)��}�(h�)https://bitbucket.org/yourname/hydrotools�h]�h�)https://bitbucket.org/yourname/hydrotools�����}�(hhZhhhNhNubah}�(h!]�h#]�h%]�h']�h)]��refuri�h\uh+hEhh=ubh�s (where yourname
represents your BitBucket/Atlassian username). Now, clone that repository onto your local
machine:�����}�(hh=hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK	hhhhubh	�literal_block���)��}�(h�3git clone https://bitbucket.org/yourname/hydrotools�h]�h�3git clone https://bitbucket.org/yourname/hydrotools�����}�hhusbah}�(h!]�h#]�h%]�h']�h)]��force���highlight_args�}��	xml:space��preserve��language��shell�uh+hshh,hKhhhhubh.)��}�(h��You will need the version control system git. If you want to update to include changes in the main
repository, you need to sync your fork on github and then pull the changes to your local clone,�h]�h��You will need the version control system git. If you want to update to include changes in the main
repository, you need to sync your fork on github and then pull the changes to your local clone,�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubht)��}�(h�git pull�h]�h�git pull�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]��force���highlight_args�}�h�h�h��shell�uh+hshh,hKhhhhubh.)��}�(h��You will also need to manually include Hydrotools in your ``$PYTHONPATH`` variable, for
example by adding this command to your shell's initialization script (e.g., ``bashrc``):�h]�(h�:You will also need to manually include Hydrotools in your �����}�(hh�hhhNhNubh	�literal���)��}�(h�``$PYTHONPATH``�h]�h�$PYTHONPATH�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubh�] variable, for
example by adding this command to your shell’s initialization script (e.g., �����}�(hh�hhhNhNubh�)��}�(h�
``bashrc``�h]�h�bashrc�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubh�):�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubht)��}�(h�7export PYTHONPATH=$PYTHONPATH:/Users/me/code/hydrotools�h]�h�7export PYTHONPATH=$PYTHONPATH:/Users/me/code/hydrotools�����}�hh�sbah}�(h!]�h#]�h%]�h']�h)]��force���highlight_args�}�h�h�h��shell�uh+hshh,hKhhhhubh.)��}�(h��where the path is, of course, replaced with the location of Hydrotools on your system. Hydrotools
relies on the Colossus package. Please see the
`Colossus Documentation <https://bdiemer.bitbucket.io/colossus/>`_ for installation instructions.�h]�(h��where the path is, of course, replaced with the location of Hydrotools on your system. Hydrotools
relies on the Colossus package. Please see the
�����}�(hh�hhhNhNubhF)��}�(h�B`Colossus Documentation <https://bdiemer.bitbucket.io/colossus/>`_�h]�h�Colossus Documentation�����}�(hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]��name��Colossus Documentation��refuri��&https://bdiemer.bitbucket.io/colossus/�uh+hEhh�ubh	�target���)��}�(h�) <https://bdiemer.bitbucket.io/colossus/>�h]�h}�(h!]��colossus-documentation�ah#]�h%]��colossus documentation�ah']�h)]��refuri�j	  uh+j
  �
referenced�Khh�ubh� for installation instructions.�����}�(hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK#hhhhubh.)��}�(hX�  To run hydrotools, it needs to know the file paths of the simulations you want to access. If you
are running on a new machine (where hydrotools has not been run before), you need to add a
:class:`~hydrotools.illustris.illustris_common.Machine` entry in the ``machines`` dictionary in
``hydrotools/illustris/illustris_common.py``. This
object typically only contains the root path to the simulation distribution, within which the
filenames and file paths are assumed to follow the original distribution. To test whether the
code works, you execute a minimal command, e.g., to extract a single catalog field from 100
randomly selected galaxies in IllustrisTNG100-3:�h]�(h��To run hydrotools, it needs to know the file paths of the simulations you want to access. If you
are running on a new machine (where hydrotools has not been run before), you need to add a
�����}�(hj$  hhhNhNubh �pending_xref���)��}�(h�7:class:`~hydrotools.illustris.illustris_common.Machine`�h]�h�)��}�(hj0  h]�h�Machine�����}�(hj2  hhhNhNubah}�(h!]�h#]�(�xref��py��py-class�eh%]�h']�h)]�uh+h�hj.  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc��installation��	refdomain�j=  �reftype��class��refexplicit���refwarn���	py:module�N�py:class�N�	reftarget��-hydrotools.illustris.illustris_common.Machine�uh+j,  hh,hK'hj$  ubh� entry in the �����}�(hj$  hhhNhNubh�)��}�(h�``machines``�h]�h�machines�����}�(hjW  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj$  ubh� dictionary in
�����}�(hj$  hhhNhNubh�)��}�(h�,``hydrotools/illustris/illustris_common.py``�h]�h�(hydrotools/illustris/illustris_common.py�����}�(hji  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj$  ubhXO  . This
object typically only contains the root path to the simulation distribution, within which the
filenames and file paths are assumed to follow the original distribution. To test whether the
code works, you execute a minimal command, e.g., to extract a single catalog field from 100
randomly selected galaxies in IllustrisTNG100-3:�����}�(hj$  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK'hhhhubht)��}�(hXL  from hydrotools.interface import interface as iface_run

iface_run.extractGalaxyData(num_processes = 1, machine_name = '****',
    sim = 'tng75-3', snap_idx = 99, Mstar_min = 1E10, Mstar_max = None,
    randomize_order = True, rank_by = 'random', n_max_extract = 100,
    catsh_get = True, catsh_fields = ['SubhaloHalfmassRadType'])�h]�hXL  from hydrotools.interface import interface as iface_run

iface_run.extractGalaxyData(num_processes = 1, machine_name = '****',
    sim = 'tng75-3', snap_idx = 99, Mstar_min = 1E10, Mstar_max = None,
    randomize_order = True, rank_by = 'random', n_max_extract = 100,
    catsh_get = True, catsh_fields = ['SubhaloHalfmassRadType'])�����}�hj�  sbah}�(h!]�h#]�h%]�h']�h)]��force���highlight_args�}�h�h�h��python�uh+hshh,hK0hhhhubh.)��}�(hX&  where you insert to correct ``machine_name`` that you created, if necessary. This command should
produce some console output and an hdf5 file with your output data. Note that you might want to
adjust the ``sim`` and ``snap_idx`` parameters, depending on which files are present on your
machine.�h]�(h�where you insert to correct �����}�(hj�  hhhNhNubh�)��}�(h�``machine_name``�h]�h�machine_name�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  ubh�� that you created, if necessary. This command should
produce some console output and an hdf5 file with your output data. Note that you might want to
adjust the �����}�(hj�  hhhNhNubh�)��}�(h�``sim``�h]�h�sim�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  ubh� and �����}�(hj�  hhhNhNubh�)��}�(h�``snap_idx``�h]�h�snap_idx�����}�(hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj�  ubh�B parameters, depending on which files are present on your
machine.�����}�(hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK9hhhhubeh}�(h!]��installation�ah#]�h%]��installation�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���image_loading��link��embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j�  j�  j  j  u�	nametypes�}�(j�  �j  �uh!}�(j�  hj  j  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.