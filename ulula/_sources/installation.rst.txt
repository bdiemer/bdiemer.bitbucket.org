===================================================================================================
Installation
===================================================================================================

Ulula is, in principle, compatible with both Python 2.7 and Python 3.x. However, the code is 
developed and tested in Python 3, which is the recommended version. 

If you are not planning on modifying the code, you can install Ulula using pip:
    
.. code:: shell

    pip install ulula

Alternatively, you can clone Ulula directly from the public BitBucket repository. However, if you are
planning to play with the code, you might want to commit your changes to your own repository, Thus, 
the recommended workflow is as follows. First, on the BitBucket website, fork the repository 
(https://bitbucket.org/bdiemer/ulula); you should end up with a repository called 
https://bitbucket.org/yourname/ulula (where yourname represents your BitBucket/Atlassian username). 
Now, clone that repository onto your local machine:

.. code:: shell

    git clone https://bitbucket.org/yourname/ulula

You will need the version control system git. If you want to update to include changes in the main
repository, you need to sync your fork on github and then pull the changes to your local clone,

.. code:: shell

    git pull

You will also need to manually include Ulula in your ``$PYTHONPATH`` variable, for 
example by adding this command to your shell's initialization script (e.g., ``bashrc``):

.. code:: shell
   
   export PYTHONPATH=$PYTHONPATH:/Users/me/code/ulula

where the path is, of course, replaced with the location of Ulula on your system.