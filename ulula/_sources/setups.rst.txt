===================================================================================================
Problem setups
===================================================================================================

In Ulula, a problem setup is an object derived from the :class:`~ulula.core.setup_base.Setup` class.
A number of 1D and 2D setups are provided as examples. 

In general, the setups are meant to be adapted and changed. For example, most setups work in cgs 
units by default, but the setup and/or the user can choose different units.

.. toctree::
    :maxdepth: 2

    setups_base
    setups_1d
    setups_2d
