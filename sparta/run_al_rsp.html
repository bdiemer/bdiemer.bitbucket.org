<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Splashback radius analysis &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Analyses" href="run_al.html" >
    <link rel="next" title="Density profile analysis" href="run_al_prf.html" >
    <link rel="prev" title="Analyses" href="run_al.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="run.html" >Running SPARTA</a></li>
          <li class="active"><a href="run_al.html" accesskey="U">Analyses</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="run_al_prf.html" title="Density profile analysis"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="run_al.html" title="Analyses"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="splashback-radius-analysis">
<h1>Splashback radius analysis<a class="headerlink" href="#splashback-radius-analysis" title="Permalink to this headline">¶</a></h1>
<p>The splashback analysis computes the splashback radius and mass of a halo over time based on the
<a class="reference internal" href="run_rs_sbk.html"><span class="doc">Splashback results</span></a> of individual particles.</p>
<p class="rubric">Algorithm</p>
<p>The splashback analysis is run at the end of a halo’s life, when all splashback events have been
collected. First, we exclude particles that belonged to subhalos greater than some maximum
sub-to-host mass ratio (the <code class="docutils literal notranslate"><span class="pre">anl_rsp_max_smr</span></code> parameter) because they are, on average,
influenced by the dynamical friction
that large subhalos experience. We use the particles’ <a class="reference internal" href="run_rs_ifl.html"><span class="doc">Infall results</span></a> to determine whether they
were part of a large subhalo.</p>
<p>After removing such particles, the radial distribution of splashback events tends to be well fit
by a Gaussian profile with a tail toward high radii. We smooth the contribution from each splashback
event in time using a Gaussian with a width of <code class="docutils literal notranslate"><span class="pre">anl_rsp_sigma_tdyn</span></code> dynamical times. We now have,
for each snapshot, a set of weighted particle splashback events. We calculate splashback properties
for this time if the accumlated weight is at least <code class="docutils literal notranslate"><span class="pre">anl_rsp_min_weight</span></code>. Possible definitions
include the mean of the distribution or precentiles. We note that, especially for higher percentiles, the
statistical uncertainty due to the limited number of particle splashback events can become
significant. To estimate such errors, we run a few hundred bootstrap samples (if requested by the
user).</p>
<p>Finally, we need to correct for two biases that occur at the end of the simulation (typically at
z = 0). First, the number of splashback events in the final time bin (between the second-to-last
and last snapshots) is drastically lower than in the previous time bins. Thus, we ignore any
splashback events that have occurred after the time of the second-to-last snapshot as they are likely
biased in some nontrivial way. Second, the distribution of splashback events considered in the final
snapshots becomes asymmetric due to the smoothing discussed above: the Gaussian filter is sensitive
to events at earlier times, but there are no events at later times. This asymmetry can lead to a
significant and systematic bias because the splashback radius is, in most cases, increasing with time.
We correct for this asymmetry by linearly extrapolating the past evolution of the splashback radius
into the future.</p>
<p>We find the splashback mass by considering the distribution of enclosed masses of the particle
splashback events. The algorithm is described in detail in
<a class="reference external" href="https://ui.adsabs.harvard.edu//#abs/2017ApJS..231....5D/abstract">Diemer 2017</a>.</p>
<p class="rubric">Splashback definitions</p>
<p>The splashback analysis can compute a number of different definitions of the splashback radius and
mass. These can be set using the <code class="docutils literal notranslate"><span class="pre">anl_rsp_defs</span></code> parameter. Of course, only definitions that can
actually be computed by the Rsp analysis can be chosen. Those include:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">Rsp-apr-mn</span></code>: mean of the apocenter distribution</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">Rsp-apr-p50</span></code>: median of the apocenter distribution</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">Rsp-apr-p&lt;n&gt;</span></code>: any percentile of the apocenter distribution</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">Msp-***</span></code>: the corresponding masses</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">***_err</span></code>: the one-sigma uncertainty on any of the above quantities. For percentiles, this
leads to bootstrapping and thus a significant performance penalty.</p></li>
</ul>
<p>For details on the string format and possible definitions, see <a class="reference internal" href="intro_conventions_halodef.html"><span class="doc">Halo radius and mass definitions</span></a>.</p>
<p class="rubric">Compile-time parameters</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 33%" />
<col style="width: 67%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Parameter</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_RSP</span></code></p></td>
<td><p>Write Rsp analyses to output file</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">ANALYSIS_RSP_MAX_SNAPS</span></code></p></td>
<td><p>The max. num. of snapshots for which analysis can be saved; must be at least num. of requested redshifts</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">ANALYSIS_RSP_MAX_DEFINITIONS</span></code></p></td>
<td><p>The maximum number of splashback definitions (radii, masses etc) that can be requested by the user</p></td>
</tr>
</tbody>
</table>
<p>If memory is an issue, the <code class="docutils literal notranslate"><span class="pre">ANALYSIS_RSP_MAX_SNAPS</span></code> and <code class="docutils literal notranslate"><span class="pre">ANALYSIS_RSP_MAX_DEFINITIONS</span></code>
parameters should be adjusted close to the values they must have to accommodate a given simulation
and user preferences.</p>
<p class="rubric">Run-time parameters</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 24%" />
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 61%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Parameter</p></th>
<th class="head"><p>Type</p></th>
<th class="head"><p>Default</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_redshifts</span></code></p></td>
<td><p>list</p></td>
<td><p>-1</p></td>
<td><p>A list of redshifts where splashback properties should be computed, or -1 (all)</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_defs</span></code></p></td>
<td><p>list</p></td>
<td><p>None</p></td>
<td><p>A list of splashback definitions to be computed (see above)</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_min_rrm</span></code></p></td>
<td><p>float</p></td>
<td><p>0.0</p></td>
<td><p>Minimum r / R200m of splashback events to use in Rsp analysis</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_max_rrm</span></code></p></td>
<td><p>float</p></td>
<td><p>1.0</p></td>
<td><p>Maximum r / R200m of splashback events to use in Rsp analysis</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_min_smr</span></code></p></td>
<td><p>float</p></td>
<td><p>-2.0</p></td>
<td><p>Minimum sub-to-host ratio to use in Rsp analysis (for subhalo particles)</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_max_smr</span></code></p></td>
<td><p>float</p></td>
<td><p>0.01</p></td>
<td><p>Maximum sub-to-host ratio to use in Rsp analysis (for subhalo particles)</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_demand_infall_rs</span></code></p></td>
<td><p>bool</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">FALSE</span></code></p></td>
<td><p>Only use splashback results if they have a corresponding infall result</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_sigma_tdyn</span></code></p></td>
<td><p>float</p></td>
<td><p>0.2</p></td>
<td><p>The width in time over which events are smoothed, in units of the dynamical time</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_min_weight</span></code></p></td>
<td><p>float</p></td>
<td><p>10.0</p></td>
<td><p>The minimum total weight in a bin for Rsp to be estimated</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_n_bootstrap</span></code></p></td>
<td><p>int</p></td>
<td><p>200</p></td>
<td><p>The number of bootstrap samples used to estimate the statistical uncertainty</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_rsp_do_correction</span></code></p></td>
<td><p>bool</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">TRUE</span></code></p></td>
<td><p>Correct the final snapshots for the asymmetry in events (recommended)</p></td>
</tr>
</tbody>
</table>
<p>Note that choosing redshifts using the <code class="docutils literal notranslate"><span class="pre">anl_rsp_redshifts</span></code> parameter saves memory and disk space,
but can lead to issues when creating halo catalogs with MORIA. If in doubt, it is probably best to
output the analysis for all snapshots.</p>
<p class="rubric">Output fields</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 14%" />
<col style="width: 5%" />
<col style="width: 15%" />
<col style="width: 22%" />
<col style="width: 44%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Field</p></th>
<th class="head"><p>Type</p></th>
<th class="head"><p>Dimensions</p></th>
<th class="head"><p>Exists if</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">halo_first</span></code></p></td>
<td><p>int64</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_halos</span></code></p></td>
<td><p>Always</p></td>
<td><p>The index of the first analysis for each halo (or -1 if none exists for a halo).</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">halo_n</span></code></p></td>
<td><p>int32</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_halos</span></code></p></td>
<td><p>Always</p></td>
<td><p>The number of analyses of this type for each halo (can be 0).</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">halo_id</span></code></p></td>
<td><p>int64</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_al_rsp</span></code></p></td>
<td><p>Always</p></td>
<td><p>The (original, first-snapshot) halo ID to which this analysis refers.</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">status</span></code></p></td>
<td><p>int8</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_al_rsp</span></code> * <code class="docutils literal notranslate"><span class="pre">n_snaps</span></code></p></td>
<td><p>Always</p></td>
<td><p>A status field that indicates whether the Rsp analysis was successful (see below)</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">&lt;halo</span> <span class="pre">definition&gt;</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_al_rsp</span></code> * <code class="docutils literal notranslate"><span class="pre">n_snaps</span></code></p></td>
<td><p>Always</p></td>
<td><p>One field for each halo definition (see above)</p></td>
</tr>
</tbody>
</table>
<p>Here, <code class="docutils literal notranslate"><span class="pre">n_snaps</span></code> is, of course, the number of redshifts chosen by the user of the number of
snapshots in the simulation if <code class="docutils literal notranslate"><span class="pre">anl_rsp_redshifts</span></code> is -1. The status field can take on the
following values:</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 5%" />
<col style="width: 26%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Value</p></th>
<th class="head"><p>Parameter</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>0</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_RSP_STATUS_UNDEFINED</span></code></p></td>
<td><p>Placeholder, should never occur in output file</p></td>
</tr>
<tr class="row-odd"><td><p>1</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_RSP_STATUS_SUCCESS</span></code></p></td>
<td><p>The analysis succeeded, all output values can be used</p></td>
</tr>
<tr class="row-even"><td><p>2</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_RSP_STATUS_HALO_NOT_VALID</span></code></p></td>
<td><p>Halo could not be analyzed at this snapshot, e.g. because too young</p></td>
</tr>
<tr class="row-odd"><td><p>3</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_RSP_STATUS_HALO_NOT_SAVED</span></code></p></td>
<td><p>Halo was not saved to the SPARTA output file at all (used in MORIA)</p></td>
</tr>
<tr class="row-even"><td><p>4</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_RSP_STATUS_NOT_FOUND</span></code></p></td>
<td><p>Analysis not found for this halo (used in MORIA)</p></td>
</tr>
<tr class="row-odd"><td><p>5</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_RSP_STATUS_INSUFFICIENT_EVENTS</span></code></p></td>
<td><p>There were not enough particle splashback events</p></td>
</tr>
<tr class="row-even"><td><p>6</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_RSP_STATUS_INSUFFICIENT_WEIGHT</span></code></p></td>
<td><p>There were enough particle splashback events, but their weight was too low</p></td>
</tr>
</tbody>
</table>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="run_al.html"
                          title="previous chapter">Analyses</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="run_al_prf.html"
                          title="next chapter">Density profile analysis</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>