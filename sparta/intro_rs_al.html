<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Results &amp; Analyses &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Introduction" href="intro.html" >
    <link rel="next" title="Code framework" href="intro_framework.html" >
    <link rel="prev" title="Tracers" href="intro_tracers.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="intro.html" accesskey="U">Introduction</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="intro_framework.html" title="Code framework"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="intro_tracers.html" title="Tracers"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="results-analyses">
<h1>Results &amp; Analyses<a class="headerlink" href="#results-analyses" title="Permalink to this headline">¶</a></h1>
<p>The previous pages about <a class="reference internal" href="intro_halos.html"><span class="doc">Halos &amp; Subhalos</span></a> and <a class="reference internal" href="intro_tracers.html"><span class="doc">Tracers</span></a> described mechanisms that are
fundamental to the SPARTA framework. In contrast, this page describes two types of components that
are meant to be flexible, meaning that</p>
<ul class="simple">
<li><p>they can easily be turned on or off by the user with compiler switches</p></li>
<li><p>they represent plug-in like structures that can easily be added to the code</p></li>
<li><p>they provide abstracted routines and data structures that allow SPARTA to be agnostic as to what
exactly they do.</p></li>
</ul>
<p>In particular, tracer results are modules and the resulting units of information that refer to an
individual tracer. Halo analyses are modules and resulting units of information that refer to
individual halos. We will discuss the currently implemented instantiations of these types of modules
in <a class="reference internal" href="run_rs.html"><span class="doc">Results</span></a> and <a class="reference internal" href="run_al.html"><span class="doc">Analyses</span></a>.</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<a class="reference internal image-reference" href="_images/intro_rs_al.jpeg"><img alt="_images/intro_rs_al.jpeg" class="align-center" src="_images/intro_rs_al.jpeg" style="width: 512.0px; height: 384.0px;" /></a>
<div class="line-block">
<div class="line"><br /></div>
</div>
<p>Here, we briefly discuss their general logic and properties. The schematic above illustrates the
main principles based on a few of the module implementations. The gray shaded area represents the
halo object in memory and code, whereas the circle represents the physical halo in the simulation.</p>
<section id="tracer-results">
<h2>Tracer results<a class="headerlink" href="#tracer-results" title="Permalink to this headline">¶</a></h2>
<p>We use the terms “event”, “tracer result”, and simply “result” interchangeably to mean pieces of
information derived from the trajectory of a tracer, as well as the routines that perform those
computations. Results are structured as plug-ins, making it easy to add a result to SPARTA (for
details, see <a class="reference internal" href="developers.html"><span class="doc">For developers</span></a>). Results are turned on and off by compiler switches.</p>
<p>In the schematic above, two results are turned on, they are represented as blue spheres and red
squares, respectively (they happen at infall and splashback, but that does not matter here).
The result routines scan each tracer’s trajectory at each snapshot. If an event is detected,
that result is saved into the result arrays of the respective tracer types (particles and
subhalos).</p>
<p>In principle, a tracer can create any number of results, though many events (such as infall
and splashback), occur at most once for each tracer in a given halo. Results are stored in
separate arrays but carry the ID of their generating tracer so that they can be reconnected
to each other later. For example, some results are continuously modified.</p>
</section>
<section id="halo-analyses">
<h2>Halo analyses<a class="headerlink" href="#halo-analyses" title="Permalink to this headline">¶</a></h2>
<p>Analyses are pieces of code that is executed on a per-halo basis. The analysis chooses when to
act. For example, the profile analysis may be called at user-defined redshifts, whereas the
splashback radius (Rsp) analysis runs only once at the end of a halo’s life.</p>
<p>Analyses have access to the full information in the halo: the particles within the search radius,
the tracer objects, and the stored results. The user can choose which elements are saved to disk
at the end of a run. Storing all results might lead to an excessive file size, and the user might
choose to only save the results of an analysis instead. This choice would not influence the way
the results are computed internally.</p>
</section>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Results &amp; Analyses</a><ul>
<li><a class="reference internal" href="#tracer-results">Tracer results</a></li>
<li><a class="reference internal" href="#halo-analyses">Halo analyses</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="intro_tracers.html"
                          title="previous chapter">Tracers</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="intro_framework.html"
                          title="next chapter">Code framework</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>