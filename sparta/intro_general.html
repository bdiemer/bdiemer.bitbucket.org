<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>What is SPARTA? &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Introduction" href="intro.html" >
    <link rel="next" title="Halos &amp; Subhalos" href="intro_halos.html" >
    <link rel="prev" title="Introduction" href="intro.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="intro.html" accesskey="U">Introduction</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="intro_halos.html" title="Halos &amp; Subhalos"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="intro.html" title="Introduction"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="what-is-sparta">
<h1>What is SPARTA?<a class="headerlink" href="#what-is-sparta" title="Permalink to this headline">¶</a></h1>
<p>SPARTA is an analysis framework for particle-based simulations of structure formation. While many
results can quickly be extracted using python scripts, there are some analyses that demand loading
very large datasets and/or heavy computation. For example, any dynamical analysis of the
trajectories of individual particles will fall under this category. SPARTA provides a
general framework for such calculations.</p>
<section id="why-sparta">
<h2>Why SPARTA?<a class="headerlink" href="#why-sparta" title="Permalink to this headline">¶</a></h2>
<p>SPARTA essentially provides a flexible workflow for analyzing large amounts of data. If you
implement a certain analysis within the SPARTA framework, the code takes care of the heavy lifting:
reading particle data and halo catalogs, parallelizing the work over an arbitrary number of
processes, load balancing, and saving the data into a (possibly very large) HDF5 file.</p>
<p>SPARTA works in a forward fashion, i.e. a SPARTA run begins at the first snapshot of
a simulation where halos exist, and moves forward snapshot by snapshot.</p>
<p>Perhaps surprisingly, SPARTA is not is a halo finder: it relies on the results of other halo
finders that need to be run before SPARTA can be run.
SPARTA then combines the halo finder output with the original particle
data, as shown in the schematic below.</p>
<a class="reference internal image-reference" href="_images/sparta_schematic.jpeg"><img alt="_images/sparta_schematic.jpeg" class="align-center" src="_images/sparta_schematic.jpeg" style="width: 512.0px; height: 208.0px;" /></a>
<p>The outputs from SPARTA can take on a variety of forms,
e.g., results that refer to individual particles or the results of computations that refer to
individual halos. Regardless of their type, all results are stored in an HDF5 file. SPARTA includes
a python package that reads these output files.</p>
<p>Due to the large variety of possible outputs, the results
are not exactly in the form of a halo catalog, which is often the desired final data product.
Moreover, SPARTA does not duplicate input from the original halo catalogs. To produce more
convenient outputs, the MORIA tool combines SPARTA output with the original halo finder results and
can output flexible, user-defined enhanced halo catalogs, either in the original halo finder format
or as HDF5 files (see <a class="reference internal" href="analysis_moria.html"><span class="doc">Creating halo catalogs with MORIA</span></a>). The reason why MORIA is separate from SPARTA is that
running SPARTA can take hours, whereas MORIA is typically quick. Moreover, the results from a
single SPARTA run can be used in very diverse output catalogs chosen by the user.</p>
</section>
<section id="fundamental-components">
<h2>Fundamental components<a class="headerlink" href="#fundamental-components" title="Permalink to this headline">¶</a></h2>
<p>The architecture of SPARTA is based around a few fundamental concepts, the most important of which
are listed here and described in more detail in the following pages:</p>
<ul class="simple">
<li><p><a class="reference internal" href="intro_halos.html"><span class="doc">Halos &amp; Subhalos</span></a>: SPARTA tracks all halos (host and sub) in a halo catalog through time, i.e.
it holds one
object per halo. Halos are the main object in Sparta, they contain basically all other
information. A halo always lives on one process, but halos can be exchanged between processes.</p></li>
<li><p><a class="reference internal" href="intro_tracers.html"><span class="doc">Tracers</span></a>: A tracer object represents a dynamical tracer such as a particle
or subhalo. Each tracer has a trajectory with repect to the halo center, i.e., a time series
of position in phase space relative to the halo. Tracers are created and destroyed according to
certain rules that depend on the types of analysis to be performed on them. A tracer can exist
in multiple halos at the same time.</p></li>
<li><p><a class="reference internal" href="intro_rs_al.html"><span class="doc">Results &amp; Analyses</span></a>: In SPARTA, the term “result” refers to information that is specific to a
tracer. For example, an infall result records information about a tracer when and where a tracer
entered a halo, an orbit counter result contains information about the number of orbits the
tracer has completed. In some cases, the term “event” would be more accurate, but for simplicity
we will stick to “result”. Similar to results, analyses compute and record information, but
they are specific
to a halo rather than a tracer. Analyses can be executed while the halo is analyzed, after a
snapshot’s work, or at the end of the run. They typically use tracer results to compute halo-wide
quantities such as the splashback radius or density profiles.</p></li>
</ul>
<p>As these categories are ubiquitous in the SPARTA code and its data products, each has long and
short idenfitiers that are used interchangeably.</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 41%" />
<col style="width: 30%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Type</p></th>
<th class="head"><p>Long name</p></th>
<th class="head"><p>Abbreviation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>Tracer</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">tracer</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">tcr</span></code></p></td>
</tr>
<tr class="row-odd"><td><p>Result</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">result</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">res</span></code></p></td>
</tr>
<tr class="row-even"><td><p>Analysis</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">analysis</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">anl</span></code></p></td>
</tr>
</tbody>
</table>
<p>Furthermore, the various incarnations of these categories also carry three-letter abbreviations
that are used throughout the code and its output, and that are interchangeable with the long names.
For example, a directory in the output file that contains information about splashback results
for particle tracers might be called <code class="docutils literal notranslate"><span class="pre">tcr_ptl/res_sbk</span></code>. However, we are getting ahead of
ourselves; the main components and their implementations are discussed in detail in the following
documentation pages.</p>
</section>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">What is SPARTA?</a><ul>
<li><a class="reference internal" href="#why-sparta">Why SPARTA?</a></li>
<li><a class="reference internal" href="#fundamental-components">Fundamental components</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="intro.html"
                          title="previous chapter">Introduction</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="intro_halos.html"
                          title="next chapter">Halos &amp; Subhalos</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>