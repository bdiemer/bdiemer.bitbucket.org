<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Infall results &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Results" href="run_rs.html" >
    <link rel="next" title="Splashback results" href="run_rs_sbk.html" >
    <link rel="prev" title="Results" href="run_rs.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="run.html" >Running SPARTA</a></li>
          <li class="active"><a href="run_rs.html" accesskey="U">Results</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="run_rs_sbk.html" title="Splashback results"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="run_rs.html" title="Results"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="infall-results">
<h1>Infall results<a class="headerlink" href="#infall-results" title="Permalink to this headline">¶</a></h1>
<p>As the name suggests, infall results record the time and circumstances of the first infall of a
tracer into a halo. These events can contain more information such as the radial and tangential
velocities at infall, as well as whether a tracer was part of a subhalo.
Specifically, infall results are created at the time when the tracer crosses R<sub>200m</sub> of a
host halo. However, an infall result is also written in cases where the true infall cannot be
determined.</p>
<p class="rubric">Particle tracers</p>
<p>In the ideal (and most common) case, SPARTA begins tracking particles well outside of
R<sub>200m</sub> (as determined by the tracer creation radius). The code then follows the particle’s
trajectory and determines the exact infall time t<sub>ifl</sub> by linear interpolation.
There are, however, a number of reasons why the moment of crossing R<sub>200m</sub> cannot be
determined:</p>
<ul class="simple">
<li><p>the particle is already in the halo when it is born, that is, when it first appears in the halo
catalog. This inevitably happens to the first particles the halo finder detects as a new halo.</p></li>
<li><p>when a halo becomes a subhalo, we determine a set of particles that truly belong to the
subhalo (see <a class="reference internal" href="intro_halos.html"><span class="doc">Halos &amp; Subhalos</span></a>) and create tracers for those particles. In some cases, those
particles might not have crossed R<sub>200m</sub> and might thus not have an infall result
associated with them.</p></li>
</ul>
<p>In those cases, we still save an infall event, but with the flag <code class="docutils literal notranslate"><span class="pre">born_in_halo</span></code>, which
indicates that the time of infall was not actually recorded and simply reflects the time when
the tracer object was created. Thus, every particle tracer should either already have an
infall result, or we should be looking for one because the tracer has not yet entered
R<sub>200m</sub>. As a consequence, infall events are numerous and can occupy a significant amount
of memory or output space. They should only be written to file if necessary.</p>
<p class="rubric">Subhalo tracers</p>
<p>Infall events for subhalos are recorded whenever they cross R<sub>200m</sub>. As the halo merger
trees are based on R<sub>200m</sub> as computed only from bound host halo particles, some subhalos
may already lie within the R<sub>200m</sub> of all particles when they first become subhalos.
However, infall events can be constructed from their saved trajectories in most cases. One
exception occurs when halos are newly created as subhalos: in this case, the subhalo never
technically crossed into its host, and no infall result is recorded.</p>
<p class="rubric">Compile-time parameters</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 33%" />
<col style="width: 67%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Parameter</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL</span></code></p></td>
<td><p>Write infall results to output file</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_TIME</span></code></p></td>
<td><p>Save the time of infall in output file</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_BORNINHALO</span></code></p></td>
<td><p>Save the born-in-halo flag in output file</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_SMR</span></code></p></td>
<td><p>Save the subhalo-to-host halo mass ratio in output file</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_VRV200</span></code></p></td>
<td><p>Save vr/v200m to the output file</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_VTV200</span></code></p></td>
<td><p>Save vt/v200m to the output file</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_X</span></code></p></td>
<td><p>Save the 3D position at infall to the output file</p></td>
</tr>
</tbody>
</table>
<p class="rubric">Run-time parameters</p>
<p>This result does not add any config parameters.</p>
<p class="rubric">Output fields</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 14%" />
<col style="width: 5%" />
<col style="width: 15%" />
<col style="width: 20%" />
<col style="width: 45%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Field</p></th>
<th class="head"><p>Type</p></th>
<th class="head"><p>Dimensions</p></th>
<th class="head"><p>Exists if</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">halo_first</span></code></p></td>
<td><p>int64</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_halos</span></code></p></td>
<td><p>Always</p></td>
<td><p>The index of the first result for each halo (or -1 if none exists for a halo).</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">halo_n</span></code></p></td>
<td><p>int32</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_halos</span></code></p></td>
<td><p>Always</p></td>
<td><p>The number of results of this type for each halo (can be 0).</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">tracer_id</span></code></p></td>
<td><p>int64</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_infall</span></code></p></td>
<td><p>Always</p></td>
<td><p>The ID of the tracer to which this result refers.</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">t_infall</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_infall</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_TIME</span></code></p></td>
<td><p>The time of infall in Gyr since the Big Bang; not reliable when born_in_halo</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">born_in_halo</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_infall</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_BORNINHALO</span></code></p></td>
<td><p>If True, infall of tracer was not tracked, tracer was created inside (sub-)halo</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">sub_mass_ratio</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_infall</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_SMR</span></code></p></td>
<td><p>SubMassRatio, &gt;0 for subhalos or particles that fell in with a subhalo</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">vrv200</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_infall</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_VRV200</span></code></p></td>
<td><p>The radial velocity at infall divided by v200m.</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">vtv200</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_infall</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_VTV200</span></code></p></td>
<td><p>The tangential velocity at infall divided by v200m.</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">x</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_infall</span></code> * 3</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_INFALL_X</span></code></p></td>
<td><p>The position at infall relative to the halo center (in physical kpc/h)</p></td>
</tr>
</tbody>
</table>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="run_rs.html"
                          title="previous chapter">Results</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="run_rs_sbk.html"
                          title="next chapter">Splashback results</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>