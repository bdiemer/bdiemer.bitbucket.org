�� 3      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�FAQ and troubleshooting�h]�h	�Text����FAQ and troubleshooting�����}�(hh�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�>/Users/benedito/University/code/sparta_dev/docs/source/faq.rst�hKubh	�	paragraph���)��}�(h�9Below are some commonly asked questions and explanations.�h]�h�9Below are some commonly asked questions and explanations.�����}�(hh1hh/hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�rubric���)��}�(h�5I get an error hdf5 error message about a locked file�h]�h�5I get an error hdf5 error message about a locked file�����}�(hhAhh?hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h=hhhhhh,hKubh.)��}�(h�XIn either SPARTA or MORIA, you may get an error message that looks something like this::�h]�h�WIn either SPARTA or MORIA, you may get an error message that looks something like this:�����}�(h�WIn either SPARTA or MORIA, you may get an error message that looks something like this:�hhMhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK	hhhhubh	�literal_block���)��}�(hX    #006: H5FDsec2.c line 959 in H5FD_sec2_lock(): unable to lock file, errno = 35, error message = 'Resource temporarily unavailable'
    major: File accessibility
    minor: Bad file ID accessed
*************************************          ERROR          *************************************
[   0] FILE ../tools/moria/moria_io.c, FUNCTION outputHDF5Catalog, LINE 276:
HDF5 return value -1 indicates error when creating catalog file.
***************************************************************************************************�h]�hX    #006: H5FDsec2.c line 959 in H5FD_sec2_lock(): unable to lock file, errno = 35, error message = 'Resource temporarily unavailable'
    major: File accessibility
    minor: Bad file ID accessed
*************************************          ERROR          *************************************
[   0] FILE ../tools/moria/moria_io.c, FUNCTION outputHDF5Catalog, LINE 276:
HDF5 return value -1 indicates error when creating catalog file.
***************************************************************************************************�����}�(hhhh^ubah}�(h!]�h#]�h%]�h']�h)]��	xml:space��preserve�uh+h\hh,hKhhhhubh.)��}�(hXY  This error is thrown by hdf5 if the output file (or, more generally, any hdf5 file the code is
trying to write to) is open in another application. For example, if you were inspecting the output
of your code using the HDF5View application and run the code again, it will likely throw this
error. Simply close the file in the viewer and try again.�h]�hXY  This error is thrown by hdf5 if the output file (or, more generally, any hdf5 file the code is
trying to write to) is open in another application. For example, if you were inspecting the output
of your code using the HDF5View application and run the code again, it will likely throw this
error. Simply close the file in the viewer and try again.�����}�(hhphhnhhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh>)��}�(h�1MORIA: Error when setting a zero-baryon cosmology�h]�h�1MORIA: Error when setting a zero-baryon cosmology�����}�(hh~hh|hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h=hhhhhh,hKubh.)��}�(h��When setting a cosmology with :math:`\Omega_{\rm b} = 0`, as in ``cosmo_omega_b 0.0`` in the
config file, I get the following error::�h]�(h�When setting a cosmology with �����}�(h�When setting a cosmology with �hh�hhhNhNubh	�math���)��}�(h�:math:`\Omega_{\rm b} = 0`�h]�h�\Omega_{\rm b} = 0�����}�(hhhh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubh�, as in �����}�(h�, as in �hh�hhhNhNubh	�literal���)��}�(h�``cosmo_omega_b 0.0``�h]�h�cosmo_omega_b 0.0�����}�(hhhh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubh�/ in the
config file, I get the following error:�����}�(h�/ in the
config file, I get the following error:�hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh])��}�(hXh  *************************************          ERROR          *************************************
[   0] FILE ../src/cosmology.c, FUNCTION transferFunctionEH98, LINE 630:
Found Omega_b = 0.000e+00, must be greater than zero when using EH98 transfer function.
***************************************************************************************************�h]�hXh  *************************************          ERROR          *************************************
[   0] FILE ../src/cosmology.c, FUNCTION transferFunctionEH98, LINE 630:
Found Omega_b = 0.000e+00, must be greater than zero when using EH98 transfer function.
***************************************************************************************************�����}�(hhhh�ubah}�(h!]�h#]�h%]�h']�h)]�hlhmuh+h\hh,hKhhhhubh.)��}�(hX  The Eisenstein & Hu 1998 approximation to the transfer function is used to calculate peak heights in
MORIA. If you want a zero-baryon cosmology, you can set ``cosmo_omega_b 1E-7``. In Einstein-de Sitter
cosmologies with a power-law power spectrum, zero baryons are allowed.�h]�(h��The Eisenstein & Hu 1998 approximation to the transfer function is used to calculate peak heights in
MORIA. If you want a zero-baryon cosmology, you can set �����}�(h��The Eisenstein & Hu 1998 approximation to the transfer function is used to calculate peak heights in
MORIA. If you want a zero-baryon cosmology, you can set �hh�hhhNhNubh�)��}�(h�``cosmo_omega_b 1E-7``�h]�h�cosmo_omega_b 1E-7�����}�(hhhh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�ubh�^. In Einstein-de Sitter
cosmologies with a power-law power spectrum, zero baryons are allowed.�����}�(h�^. In Einstein-de Sitter
cosmologies with a power-law power spectrum, zero baryons are allowed.�hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK"hhhhubh>)��}�(h�-MORIA: hdf5 error where object already exists�h]�h�-MORIA: hdf5 error where object already exists�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h=hhhhhh,hK'ubh.)��}�(h�.When running MORIA, I get an error like this::�h]�h�-When running MORIA, I get an error like this:�����}�(h�-When running MORIA, I get an error like this:�hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK(hhhhubh])��}�(hX�    #009: H5L.c line 1840 in H5L__link_cb(): name already exists
    major: Links
    minor: Object already exists
*************************************          ERROR          *************************************
[   0] FILE ../src/io/io_hdf5.c, FUNCTION hdf5DatasetFixed, LINE 426:
HDF5 return value -1 indicates error when creating 1D fixed dataset id.
***************************************************************************************************�h]�hX�    #009: H5L.c line 1840 in H5L__link_cb(): name already exists
    major: Links
    minor: Object already exists
*************************************          ERROR          *************************************
[   0] FILE ../src/io/io_hdf5.c, FUNCTION hdf5DatasetFixed, LINE 426:
HDF5 return value -1 indicates error when creating 1D fixed dataset id.
***************************************************************************************************�����}�(hhhj  ubah}�(h!]�h#]�h%]�h']�h)]�hlhmuh+h\hh,hK*hhhhubh.)��}�(hXb  This message from the hdf5 library is saying that the code was trying to write the dataset ``id``
twice, and that the second time naturally failed. This happens when a field is added to
``output_cat_fields`` that is being output automatically. These fields include ``id``, ``pid``, ``x``,
and ``v``; please do not add these fields to the output manually.�h]�(h�[This message from the hdf5 library is saying that the code was trying to write the dataset �����}�(h�[This message from the hdf5 library is saying that the code was trying to write the dataset �hj  hhhNhNubh�)��}�(h�``id``�h]�h�id�����}�(hhhj'  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  ubh�Y
twice, and that the second time naturally failed. This happens when a field is added to
�����}�(h�Y
twice, and that the second time naturally failed. This happens when a field is added to
�hj  hhhNhNubh�)��}�(h�``output_cat_fields``�h]�h�output_cat_fields�����}�(hhhj:  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  ubh�: that is being output automatically. These fields include �����}�(h�: that is being output automatically. These fields include �hj  hhhNhNubh�)��}�(h�``id``�h]�h�id�����}�(hhhjM  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  ubh�, �����}�(h�, �hj  hhhNhNubh�)��}�(h�``pid``�h]�h�pid�����}�(hhhj`  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  ubh�, �����}�(hj_  hj  ubh�)��}�(h�``x``�h]�h�x�����}�(hhhjr  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  ubh�,
and �����}�(h�,
and �hj  hhhNhNubh�)��}�(h�``v``�h]�h�v�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hj  ubh�8; please do not add these fields to the output manually.�����}�(h�8; please do not add these fields to the output manually.�hj  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK2hhhhubh>)��}�(h�$MORIA: cannot find a halo definition�h]�h�$MORIA: cannot find a halo definition�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h=hhhhhh,hK8ubh.)��}�(h�%If you get error messages like this::�h]�h�$If you get error messages like this:�����}�(h�$If you get error messages like this:�hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK9hhhhubh])��}�(hX`  *************************************          ERROR          *************************************
[   0] FILE ../tools/moria/moria_config.c, FUNCTION processMoriaConfig, LINE 707:
Could not find halo definition Msp-apr-p50_spa in SPARTA Rsp analysis.
***************************************************************************************************�h]�hX`  *************************************          ERROR          *************************************
[   0] FILE ../tools/moria/moria_config.c, FUNCTION processMoriaConfig, LINE 707:
Could not find halo definition Msp-apr-p50_spa in SPARTA Rsp analysis.
***************************************************************************************************�����}�(hhhj�  ubah}�(h!]�h#]�h%]�h']�h)]�hlhmuh+h\hh,hK;hhhhubh.)��}�(h��they are often simple errors or omissions in either the SPARTA or MORIA config file. Open the SPARTA
file in the HDF5View application (or another tool) and check that the desired field really exists!�h]�h��they are often simple errors or omissions in either the SPARTA or MORIA config file. Open the SPARTA
file in the HDF5View application (or another tool) and check that the desired field really exists!�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK@hhhhubeh}�(h!]��faq-and-troubleshooting�ah#]�h%]��faq and troubleshooting�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�j  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_images���embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�j�  j�  s�	nametypes�}�j�  Nsh!}�j�  hs�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.