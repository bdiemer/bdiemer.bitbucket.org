���3      �sphinx.addnodes��document���)��}�(�	rawsource�� ��children�]��docutils.nodes��section���)��}�(hhh]�(h	�title���)��}�(h�Results & Analyses�h]�h	�Text����Results & Analyses�����}�(hh�parent�h�	_document�h�source�N�line�Nuba�
attributes�}�(�ids�]��classes�]��names�]��dupnames�]��backrefs�]�u�tagname�hhhhhh�F/Users/benedito/University/code/sparta_dev/docs/source/intro_rs_al.rst�hKubh	�	paragraph���)��}�(h��The previous pages about :doc:`intro_halos` and :doc:`intro_tracers` described mechanisms that are
fundamental to the SPARTA framework. In contrast, this page describes two types of components that
are meant to be flexible, meaning that�h]�(h�The previous pages about �����}�(h�The previous pages about �hh/hhhNhNubh �pending_xref���)��}�(h�:doc:`intro_halos`�h]�h	�inline���)��}�(hh<h]�h�intro_halos�����}�(hhhh@hhhNhNubah}�(h!]�h#]�(�xref��std��std-doc�eh%]�h']�h)]�uh+h>hh:ubah}�(h!]�h#]�h%]�h']�h)]��refdoc��intro_rs_al��	refdomain�hK�reftype��doc��refexplicit���refwarn���	reftarget��intro_halos�uh+h8hh,hKhh/ubh� and �����}�(h� and �hh/hhhNhNubh9)��}�(h�:doc:`intro_tracers`�h]�h?)��}�(hhfh]�h�intro_tracers�����}�(hhhhhhhhNhNubah}�(h!]�h#]�(hJ�std��std-doc�eh%]�h']�h)]�uh+h>hhdubah}�(h!]�h#]�h%]�h']�h)]��refdoc�hW�	refdomain�hr�reftype��doc��refexplicit���refwarn��h]�intro_tracers�uh+h8hh,hKhh/ubh�� described mechanisms that are
fundamental to the SPARTA framework. In contrast, this page describes two types of components that
are meant to be flexible, meaning that�����}�(h�� described mechanisms that are
fundamental to the SPARTA framework. In contrast, this page describes two types of components that
are meant to be flexible, meaning that�hh/hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�bullet_list���)��}�(hhh]�(h	�	list_item���)��}�(h�Fthey can easily be turned on or off by the user with compiler switches�h]�h.)��}�(hh�h]�h�Fthey can easily be turned on or off by the user with compiler switches�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK	hh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubh�)��}�(h�Kthey represent plug-in like structures that can easily be added to the code�h]�h.)��}�(hh�h]�h�Kthey represent plug-in like structures that can easily be added to the code�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK
hh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubh�)��}�(h�rthey provide abstracted routines and data structures that allow SPARTA to be agnostic as to what
exactly they do.
�h]�h.)��}�(h�qthey provide abstracted routines and data structures that allow SPARTA to be agnostic as to what
exactly they do.�h]�h�qthey provide abstracted routines and data structures that allow SPARTA to be agnostic as to what
exactly they do.�����}�(hh�hh�hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhh�ubah}�(h!]�h#]�h%]�h']�h)]�uh+h�hh�hhhh,hNubeh}�(h!]�h#]�h%]�h']�h)]��bullet��*�uh+h�hh,hK	hhhhubh.)��}�(hXH  In particular, tracer results are modules and the resulting units of information that refer to an
individual tracer. Halo analyses are modules and resulting units of information that refer to
individual halos. We will discuss the currently implemented instantiations of these types of modules
in :doc:`run_rs` and :doc:`run_al`.�h]�(hX(  In particular, tracer results are modules and the resulting units of information that refer to an
individual tracer. Halo analyses are modules and resulting units of information that refer to
individual halos. We will discuss the currently implemented instantiations of these types of modules
in �����}�(hX(  In particular, tracer results are modules and the resulting units of information that refer to an
individual tracer. Halo analyses are modules and resulting units of information that refer to
individual halos. We will discuss the currently implemented instantiations of these types of modules
in �hh�hhhNhNubh9)��}�(h�:doc:`run_rs`�h]�h?)��}�(hh�h]�h�run_rs�����}�(hhhh�hhhNhNubah}�(h!]�h#]�(hJ�std��std-doc�eh%]�h']�h)]�uh+h>hh�ubah}�(h!]�h#]�h%]�h']�h)]��refdoc�hW�	refdomain�h��reftype��doc��refexplicit���refwarn��h]�run_rs�uh+h8hh,hKhh�ubh� and �����}�(h� and �hh�hhhNhNubh9)��}�(h�:doc:`run_al`�h]�h?)��}�(hj  h]�h�run_al�����}�(hhhj  hhhNhNubah}�(h!]�h#]�(hJ�std��std-doc�eh%]�h']�h)]�uh+h>hj  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc�hW�	refdomain�j   �reftype��doc��refexplicit���refwarn��h]�run_al�uh+h8hh,hKhh�ubh�.�����}�(h�.�hh�hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh	�
line_block���)��}�(hhh]�h	h��)��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�uh+hhj?  hhhh,hK �indent�K ubah}�(h!]�h#]�h%]�h']�h)]�uh+j=  hhhhhh,hKubh	�image���)��}�(h�J.. image:: ../images/intro_rs_al.jpeg
    :scale: 50 %
    :align: center
�h]�h}�(h!]�h#]�h%]�h']�h)]��scale�K2�align��center��uri��../images/intro_rs_al.jpeg��
candidates�}�h�jc  suh+jS  hhhhhh,hNubj>  )��}�(hhh]�jB  )��}�(hhh]�h}�(h!]�h#]�h%]�h']�h)]�uh+hhjf  hhhh,hK jL  K ubah}�(h!]�h#]�h%]�h']�h)]�uh+j=  hhhhhh,hKubh.)��}�(hX%  Here, we briefly discuss their general logic and properties. The schematic above illustrates the
main principles based on a few of the module implementations. The gray shaded area represents the
halo object in memory and code, whereas the circle represents the physical halo in the simulation.�h]�hX%  Here, we briefly discuss their general logic and properties. The schematic above illustrates the
main principles based on a few of the module implementations. The gray shaded area represents the
halo object in memory and code, whereas the circle represents the physical halo in the simulation.�����}�(hjz  hjx  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hKhhhhubh)��}�(hhh]�(h)��}�(h�Tracer results�h]�h�Tracer results�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK!ubh.)��}�(hXu  We use the terms "event", "tracer result", and simply "result" interchangeably to mean pieces of
information derived from the trajectory of a tracer, as well as the routines that perform those
computations. Results are structured as plug-ins, making it easy to add a result to SPARTA (for
details, see :doc:`developers`). Results are turned on and off by compiler switches.�h]�(hX:  We use the terms “event”, “tracer result”, and simply “result” interchangeably to mean pieces of
information derived from the trajectory of a tracer, as well as the routines that perform those
computations. Results are structured as plug-ins, making it easy to add a result to SPARTA (for
details, see �����}�(hX.  We use the terms "event", "tracer result", and simply "result" interchangeably to mean pieces of
information derived from the trajectory of a tracer, as well as the routines that perform those
computations. Results are structured as plug-ins, making it easy to add a result to SPARTA (for
details, see �hj�  hhhNhNubh9)��}�(h�:doc:`developers`�h]�h?)��}�(hj�  h]�h�
developers�����}�(hhhj�  hhhNhNubah}�(h!]�h#]�(hJ�std��std-doc�eh%]�h']�h)]�uh+h>hj�  ubah}�(h!]�h#]�h%]�h']�h)]��refdoc�hW�	refdomain�j�  �reftype��doc��refexplicit���refwarn��h]�
developers�uh+h8hh,hK#hj�  ubh�6). Results are turned on and off by compiler switches.�����}�(h�6). Results are turned on and off by compiler switches.�hj�  hhhNhNubeh}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK#hj�  hhubh.)��}�(hX~  In the schematic above, two results are turned on, they are represented as blue spheres and red
squares, respectively (they happen at infall and splashback, but that does not matter here).
The result routines scan each tracer's trajectory at each snapshot. If an event is detected,
that result is saved into the result arrays of the respective tracer types (particles and
subhalos).�h]�hX�  In the schematic above, two results are turned on, they are represented as blue spheres and red
squares, respectively (they happen at infall and splashback, but that does not matter here).
The result routines scan each tracer’s trajectory at each snapshot. If an event is detected,
that result is saved into the result arrays of the respective tracer types (particles and
subhalos).�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK(hj�  hhubh.)��}�(hX\  In principle, a tracer can create any number of results, though many events (such as infall
and splashback), occur at most once for each tracer in a given halo. Results are stored in
separate arrays but carry the ID of their generating tracer so that they can be reconnected
to each other later. For example, some results are continuously modified.�h]�hX\  In principle, a tracer can create any number of results, though many events (such as infall
and splashback), occur at most once for each tracer in a given halo. Results are stored in
separate arrays but carry the ID of their generating tracer so that they can be reconnected
to each other later. For example, some results are continuously modified.�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK.hj�  hhubeh}�(h!]��tracer-results�ah#]�h%]��tracer results�ah']�h)]�uh+h
hhhhhh,hK!ubh)��}�(hhh]�(h)��}�(h�Halo analyses�h]�h�Halo analyses�����}�(hj�  hj�  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+hhj�  hhhh,hK5ubh.)��}�(hX  Analyses are pieces of code that is executed on a per-halo basis. The analysis chooses when to
act. For example, the profile analysis may be called at user-defined redshifts, whereas the
splashback radius (Rsp) analysis runs only once at the end of a halo's life.�h]�hX	  Analyses are pieces of code that is executed on a per-halo basis. The analysis chooses when to
act. For example, the profile analysis may be called at user-defined redshifts, whereas the
splashback radius (Rsp) analysis runs only once at the end of a halo’s life.�����}�(hj  hj   hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK7hj�  hhubh.)��}�(hX�  Analyses have access to the full information in the halo: the particles within the search radius,
the tracer objects, and the stored results. The user can choose which elements are saved to disk
at the end of a run. Storing all results might lead to an excessive file size, and the user might
choose to only save the results of an analysis instead. This choice would not influence the way
the results are computed internally.�h]�hX�  Analyses have access to the full information in the halo: the particles within the search radius,
the tracer objects, and the stored results. The user can choose which elements are saved to disk
at the end of a run. Storing all results might lead to an excessive file size, and the user might
choose to only save the results of an analysis instead. This choice would not influence the way
the results are computed internally.�����}�(hj  hj  hhhNhNubah}�(h!]�h#]�h%]�h']�h)]�uh+h-hh,hK;hj�  hhubeh}�(h!]��halo-analyses�ah#]�h%]��halo analyses�ah']�h)]�uh+h
hhhhhh,hK5ubeh}�(h!]��results-analyses�ah#]�h%]��results & analyses�ah']�h)]�uh+h
hhhhhh,hKubah}�(h!]�h#]�h%]�h']�h)]��source�h,uh+h�current_source�N�current_line�N�settings��docutils.frontend��Values���)��}�(hN�	generator�N�	datestamp�N�source_link�N�
source_url�N�toc_backlinks��entry��footnote_backlinks�K�sectnum_xform�K�strip_comments�N�strip_elements_with_classes�N�strip_classes�N�report_level�K�
halt_level�K�exit_status_level�K�debug�N�warning_stream�N�	traceback���input_encoding��	utf-8-sig��input_encoding_error_handler��strict��output_encoding��utf-8��output_encoding_error_handler�jO  �error_encoding��utf-8��error_encoding_error_handler��backslashreplace��language_code��en��record_dependencies�N�config�N�	id_prefix�h�auto_id_prefix��id��dump_settings�N�dump_internals�N�dump_transforms�N�dump_pseudo_xml�N�expose_internals�N�strict_visitor�N�_disable_config�N�_source�h,�_destination�N�_config_files�]��file_insertion_enabled���raw_enabled�K�line_length_limit�M'�pep_references�N�pep_base_url��https://peps.python.org/��pep_file_url_template��pep-%04d��rfc_references�N�rfc_base_url��&https://datatracker.ietf.org/doc/html/��	tab_width�K�trim_footnote_reference_space���syntax_highlight��long��smart_quotes���smartquotes_locales�]��character_level_inline_markup���doctitle_xform���docinfo_xform�K�sectsubtitle_xform���embed_images���embed_stylesheet���cloak_email_addresses���section_self_link���env�Nub�reporter�N�indirect_targets�]��substitution_defs�}��substitution_names�}��refnames�}��refids�}��nameids�}�(j)  j&  j�  j�  j!  j  u�	nametypes�}�(j)  Nj�  Nj!  Nuh!}�(j&  hj�  j�  j  j�  u�footnote_refs�}��citation_refs�}��autofootnotes�]��autofootnote_refs�]��symbol_footnotes�]��symbol_footnote_refs�]��	footnotes�]��	citations�]��autofootnote_start�K�symbol_footnote_start�K �
id_counter��collections��Counter���}���R��parse_messages�]��transform_messages�]��transformer�N�include_log�]��
decoration�Nhhub.