<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Density profile analysis &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Analyses" href="run_al.html" >
    <link rel="next" title="Halo properties analysis" href="run_al_hps.html" >
    <link rel="prev" title="Splashback radius analysis" href="run_al_rsp.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="run.html" >Running SPARTA</a></li>
          <li class="active"><a href="run_al.html" accesskey="U">Analyses</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="run_al_hps.html" title="Halo properties analysis"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="run_al_rsp.html" title="Splashback radius analysis"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="density-profile-analysis">
<h1>Density profile analysis<a class="headerlink" href="#density-profile-analysis" title="Permalink to this headline">¶</a></h1>
<p>The density profile analysis outputs the mass enclosed within a set of radial bins at a
user-defined number of redshifts. One special feature of SPARTA’s profile analysis is that it uses
<a class="reference internal" href="run_rs_oct.html"><span class="doc">Orbit count results</span></a> to separate the profiles of particles on a first infall trajectory and those that
are orbiting in the halo (the so-called 1-halo term).</p>
<p class="rubric">Algorithm</p>
<p>The basic algorithm is simple: we create a set of logarithmically spaced radial bins between the limits
chosen by the user and compute the mass profile in these bins. However, things get more complicated
when we split the particle population by their dynamics. Here, we search for <a class="reference internal" href="run_rs_oct.html"><span class="doc">Orbit count results</span></a>
which should exist for all particle tracers. If the pericenter count is at least one, the particle is
part of the 1-halo term, the material orbiting inside the halo. In some cases, the orbit count is a
lower limit because the particle was already in the halo when the halo was first detected by the
halo finder. We include such particles in the 1-halo term, as they are extremely likely to have
already had at least one pericenter.</p>
<p>Density profiles are computed in the same way for subhalos and even ghosts. Here, R200m is
typically smaller than the all-particle R200m because it is measured from subhalo tracers only.
Thus, the profile may not actually reach a density of 200m anywhere. The orbit counter keeps
running for subhalo tracers, meaning that the 1-halo term has the same meaning in principle.
However, in subhalos (and especially in ghosts) we expect the tracer trajectories to be messy and
the orbit counting to be unreliable.</p>
<p>In ghost halos, the 1-halo term of the ghost is also computed from the particles’ orbit counts,
but as the tracers have typically orbited at least once, this 1-halo term is almost identical to a
profile of the ghost tracers. Since the orbit counter is not increased any more in ghosts, this
profile should be interpreted with care.</p>
<p class="rubric">Compile-time parameters</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 33%" />
<col style="width: 67%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Parameter</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_PROFILES</span></code></p></td>
<td><p>Write profile analyses to output file</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_PROFILES_ALL</span></code></p></td>
<td><p>Write the density profiles including all particles around a halo</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_PROFILES_1HALO</span></code></p></td>
<td><p>Write the density profiles of the 1-halo term, that is, only including particles that have orbited</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_PROFILES_VR</span></code></p></td>
<td><p>Write the average radial velocity profiles</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_PROFILES_SIGMAVR</span></code></p></td>
<td><p>Write the radial velocity dispersion profiles</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">ANALYSIS_PROFILES_MAX_SNAPS</span></code></p></td>
<td><p>The maximum number of snapshots that can be requested by the user</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">ANALYSIS_PROFILES_N_BINS</span></code></p></td>
<td><p>The number of radial bins in the profiles</p></td>
</tr>
</tbody>
</table>
<p class="rubric">Run-time parameters</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 24%" />
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 61%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Parameter</p></th>
<th class="head"><p>Type</p></th>
<th class="head"><p>Default</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_prf_redshifts</span></code></p></td>
<td><p>[float]</p></td>
<td><p>-1</p></td>
<td><p>Redshifts where profiles are output; list of floats, or -1 for all</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_prf_rmin</span></code></p></td>
<td><p>float</p></td>
<td><p>0.01</p></td>
<td><p>Minimum radius of bins in units of R200m</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_prf_rmax</span></code></p></td>
<td><p>float</p></td>
<td><p>3.0</p></td>
<td><p>Maximum radius of bins in units of R200m</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_prf_do_subs</span></code></p></td>
<td><p>bool</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">TRUE</span></code></p></td>
<td><p>Output profiles for subhalos (they will be influenced by the host)</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_prf_do_ghosts</span></code></p></td>
<td><p>bool</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">TRUE</span></code></p></td>
<td><p>Output profiles for ghost halos (they will be influenced by the host)</p></td>
</tr>
</tbody>
</table>
<p>Note that the number of profile redshifts must be smaller or equal than the <code class="docutils literal notranslate"><span class="pre">ANALYSIS_PROFILES_MAX_SNAPS</span></code>
define. It is recommended to give a list of redshifts rather than outputting profiles at all
snapshots because the latter can lead to large output and memory consumption.</p>
<div class="admonition note">
<p class="admonition-title">Note</p>
<p>Profiles of the 1-halo term at the final snapshot of a simulation are slightly biased low
because pericenter events are recorded one snapshot after they occur. SPARTA then corrects
profiles at the previous snapshot with those events, but that is not possible if the profile
is output at the final snapshot. Thus, it is recommended to output profiles at the
second-to-last snapshot instead.</p>
</div>
<p class="rubric">Output fields</p>
<p>Here, <code class="docutils literal notranslate"><span class="pre">n_prf_snp</span></code> is the number of snapshots at which profiles are output, determined by the
<code class="docutils literal notranslate"><span class="pre">anl_prf_redshifts</span></code> parameter listed above. <code class="docutils literal notranslate"><span class="pre">n_bins</span></code> is the number of radial bins which is
given by the <code class="docutils literal notranslate"><span class="pre">ANALYSIS_PROFILES_N_BINS</span></code> define.</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 13%" />
<col style="width: 5%" />
<col style="width: 14%" />
<col style="width: 11%" />
<col style="width: 18%" />
<col style="width: 41%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Field</p></th>
<th class="head"><p>Type</p></th>
<th class="head" colspan="2"><p>Dimensions</p></th>
<th class="head"><p>Exists if</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">halo_first</span></code></p></td>
<td><p>int64</p></td>
<td colspan="2"><p><code class="docutils literal notranslate"><span class="pre">n_halos</span></code></p></td>
<td><p>Always</p></td>
<td><p>The index of the first analysis for each halo (or -1 if none exists for a halo).</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">halo_n</span></code></p></td>
<td><p>int32</p></td>
<td colspan="2"><p><code class="docutils literal notranslate"><span class="pre">n_halos</span></code></p></td>
<td><p>Always</p></td>
<td><p>The number of analyses of this type for each halo (can be 0).</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">halo_id</span></code></p></td>
<td><p>int64</p></td>
<td colspan="2"><p><code class="docutils literal notranslate"><span class="pre">n_al_profiles</span></code></p></td>
<td><p>Always</p></td>
<td><p>The (original, first-snapshot) halo ID to which this analysis refers.</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">status</span></code></p></td>
<td><p>int8</p></td>
<td colspan="3"><p><code class="docutils literal notranslate"><span class="pre">n_al_profiles</span></code> * <code class="docutils literal notranslate"><span class="pre">n_prf_snp</span></code>                 | Always</p></td>
<td><p>A status field that indicates if the profile analysis was successful (see below)</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">M_all</span></code></p></td>
<td><p>float</p></td>
<td colspan="3"><p><code class="docutils literal notranslate"><span class="pre">n_al_profiles</span></code> * <code class="docutils literal notranslate"><span class="pre">n_prf_snp</span></code> * <code class="docutils literal notranslate"><span class="pre">n_bins</span></code>    | <code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_PROFILES_ALL</span></code></p></td>
<td><p>The total density profile (including all particles, mass enclosed within r bins).</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">M_1halo</span></code></p></td>
<td><p>float</p></td>
<td colspan="2"><p><code class="docutils literal notranslate"><span class="pre">n_al_profiles</span></code> * <code class="docutils literal notranslate"><span class="pre">n_prf_snp</span></code> * <code class="docutils literal notranslate"><span class="pre">n_bins</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_PROFILES_1HALO</span></code></p></td>
<td><p>The mass of the 1-halo term (ptls. that have undergone at least one pericenter)</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">vr</span></code></p></td>
<td><p>float</p></td>
<td colspan="2"><p><code class="docutils literal notranslate"><span class="pre">n_al_profiles</span></code> * <code class="docutils literal notranslate"><span class="pre">n_prf_snp</span></code> * <code class="docutils literal notranslate"><span class="pre">n_bins</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_PROFILES_VR</span></code></p></td>
<td><p>The average radial velocity profile</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">sigma_vr</span></code></p></td>
<td><p>float</p></td>
<td colspan="2"><p><code class="docutils literal notranslate"><span class="pre">n_al_profiles</span></code> * <code class="docutils literal notranslate"><span class="pre">n_prf_snp</span></code> * <code class="docutils literal notranslate"><span class="pre">n_bins</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_PROFILES_SIGMAVR</span></code></p></td>
<td><p>The radial velocity dispersion profile</p></td>
</tr>
</tbody>
</table>
<p>The <code class="docutils literal notranslate"><span class="pre">status</span></code> field can take on the following values:</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 5%" />
<col style="width: 26%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Value</p></th>
<th class="head"><p>Parameter</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>0</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_PRF_STATUS_UNDEFINED</span></code></p></td>
<td><p>Placeholder, should never occur in output file</p></td>
</tr>
<tr class="row-odd"><td><p>1</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_PRF_STATUS_SUCCESS</span></code></p></td>
<td><p>The analysis succeeded, all output values can be used</p></td>
</tr>
<tr class="row-even"><td><p>2</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_PRF_STATUS_HALO_NOT_VALID</span></code></p></td>
<td><p>Halo could not be analyzed at this snapshot, e.g. because too young</p></td>
</tr>
<tr class="row-odd"><td><p>5</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_PRF_STATUS_SEARCH_RADIUS</span></code></p></td>
<td><p>The search radius had to be reduced, the profile was not computed</p></td>
</tr>
<tr class="row-even"><td><p>6</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_PRF_STATUS_CATALOG_RADIUS</span></code></p></td>
<td><p>R200m_all could not be computed, the radius was taken from catalog; the profile was not computed</p></td>
</tr>
</tbody>
</table>
<p>The <code class="docutils literal notranslate"><span class="pre">ANL_PRF_STATUS_SEARCH_RADIUS</span></code> error reflects the rare case where SPARTA underestimates a
halo radius, leading to too small a particle box. The particles can then not be loaded after the
fact as that would mean re-doing a large fraction of SPARTA’s work on the snapshot. As particles
were almost certainly missed, we do not compute a profile in this case.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">ANL_PRF_STATUS_CATALOG_RADIUS</span></code> error means that R200_all was not computed. This is a rare
case where the halo is next to a (typically much larger) halo that dominates its density field.
While the bound-only mass from the catalog may be small, R200m_all would include the entirety of
the larger halo. In this case, SPARTA may fall back to the catalog radius (see the
<code class="docutils literal notranslate"><span class="pre">halo_max_radius_ratio_cat</span></code> configuration parameter).</p>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="run_al_rsp.html"
                          title="previous chapter">Splashback radius analysis</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="run_al_hps.html"
                          title="next chapter">Halo properties analysis</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>