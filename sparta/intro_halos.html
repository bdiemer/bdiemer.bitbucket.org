<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Halos &amp; Subhalos &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Introduction" href="intro.html" >
    <link rel="next" title="Tracers" href="intro_tracers.html" >
    <link rel="prev" title="What is SPARTA?" href="intro_general.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="intro.html" accesskey="U">Introduction</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="intro_tracers.html" title="Tracers"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="intro_general.html" title="What is SPARTA?"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="halos-subhalos">
<h1>Halos &amp; Subhalos<a class="headerlink" href="#halos-subhalos" title="Permalink to this headline">¶</a></h1>
<p>Halos serve as the top-level organizational unit for work and memory in SPARTA. Each halo is
uniquely assigned to one process, and halos can be exchanged between processes. A halo object
contains dynamical lists of <a class="reference internal" href="intro_tracers.html"><span class="doc">Tracers</span></a> for each tracer type, as well as
<a class="reference internal" href="intro_rs_al.html"><span class="doc">Results &amp; Analyses</span></a>.</p>
<section id="halo-and-subhalo-tracking">
<h2>Halo and subhalo tracking<a class="headerlink" href="#halo-and-subhalo-tracking" title="Permalink to this headline">¶</a></h2>
<p>SPARTA is not a halo finder, it takes its list of halos from an existing halo catalog (the most
well-tested halo finder for this purpose is Peter Behroozi’s
<a class="reference external" href="https://bitbucket.org/gfcstanford/rockstar/src/master/">ROCKSTAR</a> halo finder and his
<a class="reference external" href="https://bitbucket.org/pbehroozi/consistent-trees/src/master/">consistent-trees</a> merger trees
code). Based on those catalogs, SPARTA keeps track of all halos in a simulation.</p>
<p>At each snapshot, SPARTA connects halos to their descendants by matching their unique IDs, saving
certain halo properties, and updating others. SPARTA explicitly tracks the relation between host
and subhalos as defined by the merger trees (or catalogs). Internally, SPARTA assigns each subhalo
to its top-level host, meaning that if there are subhalos within subhalos, their parent ID is that
of the highest-level host. The schematic below demonstrates some of the most common host-sub
relations and their evolution.</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<a class="reference internal image-reference" href="_images/sparta_halos.jpeg"><img alt="_images/sparta_halos.jpeg" class="align-center" src="_images/sparta_halos.jpeg" style="width: 512.0px; height: 384.0px;" /></a>
<p>Red circles represent host halos, yellow circles subhalos. SPARTA would see this structure from top
to bottom, going forward in time. At the end of each snapshot, each halo has a certain status which
is written into the output file (see below).</p>
<p>The halo radii computed by SPARTA (e.g., splashback or spherical overdensity radii) can be used to
reassign subhalos using MORIA (see <a class="reference internal" href="analysis_moria.html"><span class="doc">Creating halo catalogs with MORIA</span></a>), but internally, SPARTA treats a halo as
a subhalo if it is so defined in the halo catalog. Thus, the definition and algorithm used for
subhalo assignment in the halo catalog matters a great deal.</p>
</section>
<section id="ghosts">
<h2>Ghosts<a class="headerlink" href="#ghosts" title="Permalink to this headline">¶</a></h2>
<p>The vast majority of halos end in one of two ways: because the simuation ends, or because they
merge into another, larger halo. Some of this merging is numerical: subhalos tend to be
artificially disrupted in N-body simulations, and there is little we can do about that after the
fact. However, the life of subhalos may also be cut short because they cannot be identified by the
halo finder any more as they are mixed with their host’s particles. Halo finders such as Rockstar
combat this issue using techniques such as phase-space FOF, but even those algorithms may fail to
follow some small subhalos.</p>
<p>In SPARTA, we have access to the full particle information, and can track subhalos using the member
particles they had at infall (see <a class="reference internal" href="intro_tracers.html"><span class="doc">Tracers</span></a> for details on how those particles are
selected and tracked). We call such halos “ghosts” as they are the remnants of a halo that does not
exist in the halo finder catalogs any longer. If the user chooses to output ghosts, SPARTA follows
them and outputs them much like normal halos. A few common scenarios are shown in this schematic:</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<a class="reference internal image-reference" href="_images/sparta_halos_ghosts.jpeg"><img alt="_images/sparta_halos_ghosts.jpeg" class="align-center" src="_images/sparta_halos_ghosts.jpeg" style="width: 512.0px; height: 384.0px;" /></a>
<p>The dashed yellow circles denote ghosts. As they are not present in the original halo catalog, they
are assigned an ID that reflects the ghost’s original ID and the snapshot number (white/gray
labels). The host-subhalo relations are a little more complicated than for normal halos, including
the following scenarios:</p>
<ul class="simple">
<li><p>When a subhalo is converted into a ghost, the latter maintains the same host halo.</p></li>
<li><p>Unlike normal halos, ghosts can be subhalos of other subhalos. The main reason is that the halo
finder is not keeping track of the ghosts for us. Thus, if a ghost’s host is temporarily a
subhalo but than becomes a host again, we would lose that “true” connection if we were to assign
it to the top-level host.</p></li>
<li><p>When the host halo of a ghost merges into another halo and disappears from the catalog, we
transfer the ghost to that new host.</p></li>
<li><p>When the host halo of a ghost becomes a ghost itself, we transfer the first ghost to the new
ghost’s subhalo, thus avoiding ghost-ghost relations.</p></li>
</ul>
<p>From the particles we are tracking in a ghost, we compute its center as the location of the
particle with the highest binding energy given all tracked particles, and its velocity as the
average velocity with R<sub>200m</sub> (see below for details on how the radius is computed).</p>
<p>There are, fundamentally, two ways for a ghost to end: it either reaches the last snapshot like
other halos, or it dissolves to the point where it cannot be identified any longer. The user can
set parameters for these criteria (see <a class="reference internal" href="run_config.html"><span class="doc">Run-time configuration parameters</span></a>), namely:</p>
<ul class="simple">
<li><p>a minimum number of particles at which we stop tracking the ghost.</p></li>
<li><p>a minimum distance from the host center and a maximum time for which this distance is tolerated.
If the ghost is close to the host center for longer, we consider it to have merged with the host.
We also consider the positional uncertainty on the ghost center in this calculation.</p></li>
</ul>
<p>The final status (see below) indicates exactly what happened to the ghost.</p>
</section>
<section id="halo-status-fields">
<h2>Halo status fields<a class="headerlink" href="#halo-status-fields" title="Permalink to this headline">¶</a></h2>
<p>The following values are possible:</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 8%" />
<col style="width: 24%" />
<col style="width: 67%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Value</p></th>
<th class="head"><p>Meaning</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">-2</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">NOT_FOUND</span></code></p></td>
<td><p>Halo not found in SPARTA file (use in tools such as MORIA)</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">-1</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">NONE</span></code></p></td>
<td><p>Halo did not exist at this snapshot</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">10</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">HOST</span></code></p></td>
<td><p>Host halo</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">20</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">SUB</span></code></p></td>
<td><p>Subhalo</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">21</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">BECOMING_SUB</span></code></p></td>
<td><p>Subhalo; became a subhalo in this snap</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">22</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">BECOMING_HOST</span></code></p></td>
<td><p>Subhalo; will become a host in the next snap</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">23</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">BECOMING_SUB_HOST</span></code></p></td>
<td><p>Subhalo; became sub this snap and will be host in the next</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">24</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">SWITCHED_HOST</span></code></p></td>
<td><p>Subhalo; switched host</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">30</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">GHOST_HOST</span></code></p></td>
<td><p>Ghost host halo</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">31</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">GHOST_SUB</span></code></p></td>
<td><p>Ghost subhalo</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">32</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">GHOST_SWITCHING</span></code></p></td>
<td><p>Ghost subhalo, changing host</p></td>
</tr>
</tbody>
</table>
<p>When the trajectory of a halo ends, it is assigned a <code class="docutils literal notranslate"><span class="pre">final_status</span></code> value. Some examples are shown
as gray boxes in the schematic above. The values can be:</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 8%" />
<col style="width: 23%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Status</p></th>
<th class="head"><p>Meaning</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">50</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">MERGED</span></code></p></td>
<td><p>The halo merged with another, larger halo</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">51</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">GHOST_CENTER</span></code></p></td>
<td><p>The ghost’s center coincided with the host’s, meaning it merged</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">52</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">GHOST_TOO_SMALL</span></code></p></td>
<td><p>The ghost was identified, but below the minimum number of particles</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">53</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">GHOST_NOT_FOUND</span></code></p></td>
<td><p>The tracer particles were not found in the search volume; should be rare</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">54</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">GHOST_POSITION</span></code></p></td>
<td><p>The ghost’s position could not clearly be identified from its tracers</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">60</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">LAST_SNAP</span></code></p></td>
<td><p>The halo ended at the last snapshot of the simulation</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">70</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">NOT_FOUND</span></code></p></td>
<td><p>A descendant was not found in the catalog (should not happen)</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">71</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">JUMP</span></code></p></td>
<td><p>The descendant in the catalog jumped unphysically (should not happen)</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">72</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">HOST_ENDED</span></code></p></td>
<td><p>This halo was a sub of a host that ended</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">73</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">SEARCH_RADIUS</span></code></p></td>
<td><p>Failed to find a valid particle distribution (should not happen)</p></td>
</tr>
</tbody>
</table>
<p>If a halo catalog is constructed in a self-consistent manner, only <code class="docutils literal notranslate"><span class="pre">MERGED</span></code> and <code class="docutils literal notranslate"><span class="pre">LAST_SNAP</span></code>
should occur, plus the ghost statuses if ghosts are on.</p>
<p>We note that SPARTA does not have to be started at the first snapshot, of a simulation but its
intended mode of operation is to follow entire halo histories.</p>
</section>
<section id="internal-halo-masses-and-radii">
<h2>Internal halo masses and radii<a class="headerlink" href="#internal-halo-masses-and-radii" title="Permalink to this headline">¶</a></h2>
<p>For many purposes, SPARTA must internally rely on some definition of the halo radius, even if the
purpose of a SPARTA run is to determine other definitions of the halo boundary. The chosen
definition for this internal boundary is R<sub>200m</sub>, but the exact details of how it is
computed differ for different types of halos. The chosen value of R<sub>200m</sub> is written to
the output file for all halos and times.</p>
<p>For host (isolated) halos, the halo catalog may already specify R<sub>200m</sub>, but SPARTA computes
it directly by constructing the mass profile around a halo from the particle distribution. If the
initial search radius is too small to find R<sub>200m</sub>, it is increased iteratively until a
solution has been found. Here, R<sub>200m</sub> are computed from the full particle distribution,
including bound and unbound particles, in order to avoid the ambiguities inherent in any
unbinding procedure. For host halos, the difference between the bound and full mass profiles is
generally small.</p>
<p>For subhalos, however, the difference can be large because the density around a subhalo may never
reach sufficiently low values, meaning that material from its host is included in the spherical
overdensity R<sub>200m</sub> which leads to an unphysically high radius and mass. There are two
solutions to this issue: we can either use an estimate of a bound-only R<sub>200m</sub> from the
halo catalog, or we can compute it from only subhalo particles that we are tracking over time
(see <a class="reference internal" href="intro_tracers.html"><span class="doc">Tracers</span></a> for details on how this is done). For many operations, including
determining the particle search radius around subhalos and the radius out to which we are tracking
particles, we use the maximum of the tracer-based and catalog-based radii to be on the safe side,
but the tracer-based radius is output.</p>
<p>If we are not tracking particles, we use the radius at infall for subhalos, that is, the last
measured R<sub>200m</sub> at the snapshot before the halo became a subhalo. This radius is less
arbitrary than halofinder-specific bound-only definitions. We note that we keep the radius constant
rather than the mass; this corresponds to a decreasing mass as the mean density of the universe
drops over cosmic time. If we kept the mass constant, the physical halo radius would increase
with time, which is unlikely to be a good representation of the physical evolution of subhalos.
However, if we are not tracking subhalo particles, we do not do much with subhalos at all and the
exact definition does not matter much.</p>
<p>For ghosts, we stay consistent with the subhalo definition described above and always use the
tracer mass, that is R<sub>200m</sub> measured from those particles that we are tracking. The radius
is measured using the tracked-particle profile around the center, which is the location of the
particle with the highest binding energy. In summary, the internal R<sub>200m</sub> is computed in
the following ways:</p>
<ul class="simple">
<li><p>Host halos: directly measured from the particle distribution, including all particles.</p></li>
<li><p>Subhalos that are subs for only one snapshot: treated like host halos.</p></li>
<li><p>Subhalos, if tracking subhalo particles is off: radius at infall, meaning the radius at the last
snapshot when the halo was a host.</p></li>
<li><p>Subhalos, if tracking subhalo particles is on: determined from tracer particles. If there are no
tracers, we use the catalog radius.</p></li>
<li><p>Ghosts: determined from tracer particles. For subhalo phantoms, the same routine can optionally
be applied.</p></li>
</ul>
<p>We emphasize that the purpose of this internal R<sub>200m</sub> calculation is not to provide a
“perfect” mass definition for halos, but rather to provide a consistent basis for the algorithms in
SPARTA. There are many other ways to calculate various mass definitions, see e.g. the
<a class="reference internal" href="run_al_rsp.html"><span class="doc">Splashback radius analysis</span></a> and <a class="reference internal" href="run_al_hps.html"><span class="doc">Halo properties analysis</span></a> modules.</p>
</section>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Halos &amp; Subhalos</a><ul>
<li><a class="reference internal" href="#halo-and-subhalo-tracking">Halo and subhalo tracking</a></li>
<li><a class="reference internal" href="#ghosts">Ghosts</a></li>
<li><a class="reference internal" href="#halo-status-fields">Halo status fields</a></li>
<li><a class="reference internal" href="#internal-halo-masses-and-radii">Internal halo masses and radii</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="intro_general.html"
                          title="previous chapter">What is SPARTA?</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="intro_tracers.html"
                          title="next chapter">Tracers</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>