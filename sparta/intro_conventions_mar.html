<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Dynamical times and mass accretion rates &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Units and conventions" href="intro_conventions.html" >
    <link rel="next" title="Acknowlegments" href="intro_acknowlegments.html" >
    <link rel="prev" title="Halo radius and mass definitions" href="intro_conventions_halodef.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="intro.html" >Introduction</a></li>
          <li class="active"><a href="intro_conventions.html" accesskey="U">Units and conventions</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="intro_acknowlegments.html" title="Acknowlegments"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="intro_conventions_halodef.html" title="Halo radius and mass definitions"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="dynamical-times-and-mass-accretion-rates">
<h1>Dynamical times and mass accretion rates<a class="headerlink" href="#dynamical-times-and-mass-accretion-rates" title="Permalink to this headline">¶</a></h1>
<p>Mass accretion rates (MARs) in SPARTA follow the definition of
<a class="reference external" href="https://ui.adsabs.harvard.edu/abs/2017ApJS..231....5D/abstract">Diemer 2017</a>,</p>
<div class="math notranslate nohighlight">
\[\Gamma(t) = \frac{\log[M(t)] - \log[M(t - t_{\rm dyn})]}{\log[a(t)] - \log[a(t - t_{\rm dyn})]}\]</div>
<p>where <span class="math notranslate nohighlight">\(t_{\rm dyn}\)</span> is the dynamical time and the mass is typically defined as
<span class="math notranslate nohighlight">\(M_{\rm 200m}\)</span> (though this does not always have to be the case). The dynamical time is based
on the typical velocity of a halo,</p>
<div class="math notranslate nohighlight">
\[v_{\Delta} \equiv \sqrt{\frac{G M_{\Delta}}{R_{\Delta}}} \,.\]</div>
<p>We use the crossing time such that</p>
<div class="math notranslate nohighlight">
\[t_{\rm dyn} \equiv t_{\rm cross} = \frac{2 R_{\Delta}}{v_{\Delta}} \,.\]</div>
<p>This time should roughly describe the time it takes a gravitational tracer to cross a halo.
This time is independent of halo mass (as long as spherical overdensity masses are used) and almost
independent of cosmology (here shown for <span class="math notranslate nohighlight">\(\Delta_{\rm 200m}\)</span>):</p>
<a class="reference internal image-reference" href="_images/conventions_tdyn.png"><img alt="_images/conventions_tdyn.png" class="align-center" src="_images/conventions_tdyn.png" style="width: 375.0px; height: 375.0px;" /></a>
<p>We note that this definition is by no means unique. Most notably, it does not quite correspond
to the definition used by Peter Behroozi’s
<a class="reference external" href="https://bitbucket.org/pbehroozi/consistent-trees/src/master/">consistent-trees</a> code because</p>
<ul class="simple">
<li><p>the definition of dynamical time differs by a factor of two, though this difference can be
avoided by using the <code class="docutils literal notranslate"><span class="pre">2Tdyn</span></code> accretion rate</p></li>
<li><p>the dynamical time is based on <span class="math notranslate nohighlight">\(\Delta_{\rm vir}\)</span> rather than <span class="math notranslate nohighlight">\(\Delta_{\rm 200m}\)</span>,
meaning the accretion rate is measured over a slightly different time interval (which makes a
surprisingly large difference for a number of halos)</p></li>
<li><p>the mass history is interpolated, whereas the nearest snapshot is taken in SPARTA</p></li>
<li><p>the bound-particle mass is used (though that is very similar to the total SO mass for almost
all isolated halos).</p></li>
</ul>
<p>For these reasons, one should not expect the two accretion rates to agree quantitatively.</p>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="intro_conventions_halodef.html"
                          title="previous chapter">Halo radius and mass definitions</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="intro_acknowlegments.html"
                          title="next chapter">Acknowlegments</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>