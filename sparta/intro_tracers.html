<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Tracers &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Introduction" href="intro.html" >
    <link rel="next" title="Results &amp; Analyses" href="intro_rs_al.html" >
    <link rel="prev" title="Halos &amp; Subhalos" href="intro_halos.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="intro.html" accesskey="U">Introduction</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="intro_rs_al.html" title="Results &amp; Analyses"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="intro_halos.html" title="Halos &amp; Subhalos"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="tracers">
<h1>Tracers<a class="headerlink" href="#tracers" title="Permalink to this headline">¶</a></h1>
<p>A dynamical tracer, hereafter simply “tracer”, is defined as an object that follows some orbit
with respect to a halo center (a “trajectory”).
Tracers can be part of multiple halos at the same time because each halo keeps a separate,
dynamically allocated array of tracers of each type. Currently, two tracers are implemented in
SPARTA, particles and subhalos, which carry the following names and abbreviations throughout the
code and output:</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 41%" />
<col style="width: 30%" />
<col style="width: 30%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Tracer type</p></th>
<th class="head"><p>Long name</p></th>
<th class="head"><p>Abbreviation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>Particles</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">particles</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ptl</span></code></p></td>
</tr>
<tr class="row-odd"><td><p>Subhalos</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">subhalos</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">sho</span></code></p></td>
</tr>
</tbody>
</table>
<p>For example, a directory for tracer particles might be called <code class="docutils literal notranslate"><span class="pre">tcr_ptl</span></code> or <code class="docutils literal notranslate"><span class="pre">tcr_sho</span></code>.</p>
<section id="particle-tracers-in-host-halos">
<h2>Particle tracers in host halos<a class="headerlink" href="#particle-tracers-in-host-halos" title="Permalink to this headline">¶</a></h2>
<p>The goal of SPARTA is to consider as complete as possible a set of particles in a halo. The full
particle tracer logic is somewhat complicated and depends on whether a halo is considered a host
or subhalo. Here, we discuss the basic logic of particle tracers as shown in the schematics below.</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<a class="reference internal image-reference" href="_images/sparta_tracers.jpeg"><img alt="_images/sparta_tracers.jpeg" class="align-center" src="_images/sparta_tracers.jpeg" style="width: 512.0px; height: 384.0px;" /></a>
<div class="line-block">
<div class="line"><br /></div>
</div>
<p>Particle tracers are created whenever a particle first comes within r<sub>create</sub> of a halo’s
center (typically about 2 R<sub>200m</sub>), and SPARTA follows its trajectory from that point onward
(see <a class="reference internal" href="run_config.html"><span class="doc">Run-time configuration parameters</span></a> for how to set r<sub>create</sub>). Due to the enormous number of particles in
a simulation, storing the full trajectories of all tracers in memory is impossible. Thus, SPARTA
keeps only a certain number of snapshots depending on the analyses to be performed. The particle
properties that are stored are adjusted depending on the chosen output variables (for example,
radius and radial velocity rather than the full three-dimensional position, unless the latter is
necessary for a particular purpose). At each snapshot, all active tracer trajectories are analyzed
for certain events, including first infall
into the halo (crossing R<sub>200m</sub>, hereafter “infall”), reaching the apocenter of its first
orbit (hereafter “splashback”), and so on (see <a class="reference internal" href="intro_rs_al.html"><span class="doc">Results &amp; Analyses</span></a>). A particle tracer’s life can
end for a number of reasons:</p>
<ul class="simple">
<li><p>if all analysess of its trajectory have finished (e.g., when the search for both an infall
and a splashback event has succeeded or failed)</p></li>
<li><p>if it strays too far from the halo to be considered a dynamical tracer of the halo potential
(r &gt; r<sub>delete</sub>, typically about 3 R<sub>200m</sub>)</p></li>
<li><p>if the halo becomes a subhalo for more than one snapshot (though that depends, see section
on subhalos below; halos that are only a subhalo for one snapshot are considered not to have
truly become a subhalo, at least not yet)</p></li>
<li><p>if the halo itself ceases to exist according to the catalog.</p></li>
</ul>
<p>If any of those occur, the tracer object is deleted from memory, and only its results remain.
If the deleted tracer had entered within R<sub>200m</sub> of the halo, we add its ID to a list of
tracers to be ignored in the future. Such a list is kept by each halo for each tracer type and
checked before creating new tracers to avoid accidentally treating a returning tracer as
infalling for the first time. This logic leads to many, many possible scenarios for the life of
a particle tracer. Some of the most common are sketched in the figure above:</p>
<ol class="arabic simple">
<li><p>A tracer enters into the creation radius and is created, but then strays far from the halo and is
deleted again. No results have been recorded, and the tracer is not added to the ignore list as
it is not deemed to ever have entered the halo.</p></li>
<li><p>A tracer enters into the halo, all chosen results (say, infall and splashback) are completed. The
tracer is deleted at that point and added to the ignore list.</p></li>
<li><p>A tracer enters the halo, leaves, is put on the ignore list, but returns. Depending on the
results chosen, the tracer may be recreated (e.g., to count its orbits). However, other results
such as splashback will be turned off because the tracer is not on a first orbit.</p></li>
<li><p>A particle enters as part of a second halo (yellow). It exists as a tracer in both halos after it
enters the creation radius. When the second halo becomes a subhalo (according to the catalog), we
determine the particles that truly belong to this subhalo (see section below), identify their
counterparts in the  host halo, and tag them as having originated from a subhalo. This is
important for certain analyses where we might not want to include tracer particles from massive
subhalos.</p></li>
</ol>
<p>Note that we cannot set r<sub>delete</sub> to arbitrarily high values because particle tracers often
dominate SPARTA’s memory consumption, and because a large search radius would force each process
to consider a large fraction of the simulation box.</p>
</section>
<section id="particle-tracers-in-subhalos">
<h2>Particle tracers in subhalos<a class="headerlink" href="#particle-tracers-in-subhalos" title="Permalink to this headline">¶</a></h2>
<p>The situation in subhalos is fundamentally different from host halos. The motion of many of a
subhalo’s particles about the subhalo center will make no sense dynamically, as the particle does
executes a trajectory in the combined potential of host and sub. Similarly, if seen from the
perspective of the host halo, tracers will also not follow normal orbits. Overall, the interaction
between host and sub can lead to vastly disparate outcomes, some of which can be very misleading
when interpreted as normal host halo particles.</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<a class="reference internal image-reference" href="_images/sparta_tracers_sub.jpeg"><img alt="_images/sparta_tracers_sub.jpeg" class="align-center" src="_images/sparta_tracers_sub.jpeg" style="width: 512.0px; height: 384.0px;" /></a>
<p>The schematic above shows three typical cases of what can happen to particles that enter a host as
part of a subhalo, although there are numerous other strange possibilties. In particular:</p>
<ol class="arabic simple">
<li><p>A subhalo particle happens to be close to pericenter (i.e., close to the host’s center) during
first orbit, and is quickly stripped from the subhalo.</p></li>
<li><p>A subhalo particle is retained by the subhalo and roughly follows it’s orbit (shows as a yellow
line).</p></li>
<li><p>A subhalo particle is relatively weakly bound to the subhalo, is stripped from it, and executes
a relatively normal orbit about the host halo.</p></li>
</ol>
<p>Because of these possibilities, we tag host tracer particles as having originated from a subhalo
at infall (see next section for how this decision is made).
The particle tracers that are identified as subhalo particles are kept within the subhalo, or
created if they do not yet exist for some reason. All other particle tracers are deleted. While a
halo is a subhalo, no new tracers can be added; the idea is that many host particles will
accidentally stray into the subhalo, but that a subhalo can only lose mass to its host. We track
the subhalo tracer particles until they reach a deletion radius set by the user (which is
similar to the deletion radius around host halos, but refers to the radius of the subhalo when it
entered the host). If the subhalo becomes a host halo again (i.e., if it is a “backsplash halo”),
we begin adding new tracers once again.</p>
</section>
<section id="which-particles-belong-to-a-subhalo">
<h2>Which particles belong to a subhalo?<a class="headerlink" href="#which-particles-belong-to-a-subhalo" title="Permalink to this headline">¶</a></h2>
<p>As mentioned above, we need to decide which particles belong to a subhalo in order to track them.
Membership in a subhalo is also important for other reasons. For example, subhalos suffer from
dynamical friction, meaning their orbit is not the same as a particle’s orbit with the same initial
conditions (this is important for the <a class="reference internal" href="run_al_rsp.html"><span class="doc">Splashback radius analysis</span></a>). Thus, we wish to tag tracer particles in
host halos with a sub-to-host mass ratio (SMR) if they fell in as part of a subhalo.</p>
<a class="reference internal image-reference" href="_images/sparta_tracers_subtagging.jpeg"><img alt="_images/sparta_tracers_subtagging.jpeg" class="align-center" src="_images/sparta_tracers_subtagging.jpeg" style="width: 512.0px; height: 384.0px;" /></a>
<p>Either way, how can we decide which particles are in a subhalo? To understand why this is a really
tricky question, consider the particle orbits shown in the schematic above. Here, the yellow
subhalo falls into the red host halo. At the point where it is first considered a subhalo, its
center has just penetrated into the radius of the host, by definition.</p>
<p>The particle orbits demonstrate
a few common cases, though there are many, many more possibilities. Particle (1) truly belongs to
the subhalo: it fell into the sub long time ago and then into the host as part of the subhalo.
For particle (2), however, the situation is much less clear: it fell into the subhalo before
falling into the host halo, but perhaps that happened by chance - it was not part of the subhalo
for most of its life. Particle (3) clearly does not belong to the subhalo, but that is not obvious
because it happens to fall into the host at a similar time and then into the subhalo. Finally,
particle (4) fell into the host long time ago and has already orbited, but nevertheless ends up
within the radius of the subhalo at infall.</p>
<p>To distinguish these possibilities in a phsyically meaningful way, SPARTA offers a number of
algorithms which can be combined as chosen by the user at compile-time
(see <a class="reference internal" href="run_compile.html"><span class="doc">Compiling SPARTA</span></a>), including:</p>
<ul class="simple">
<li><p>All particles that have been part of the halo for a particular time in units of the dynamical
time (see <a class="reference internal" href="intro_conventions_mar.html"><span class="doc">Dynamical times and mass accretion rates</span></a>). Here, SPARTA would measure the time between the infall
into the subhalo (blue dots) and the time when the subhalo falls into the host. If this time is
longer than a certain number of dynamical times (see <a class="reference internal" href="run_config.html"><span class="doc">Run-time configuration parameters</span></a>), the particle is deemed
to be part of the subhalo. This algorithm works well for most particles, but might erroneously
tag particles in subhalos with tangential (or other strange) orbits, where the subhalo might be
close to the host, add host particles, and take a long time to actually be considered a subhalo.
In that case, the particles swept up by the subhalo might be tagged.</p></li>
<li><p>Particles that joined the subhalo a certain distance from the host halo. Here, we would compare
the distance of the infall into the subhalo (blue dots) from the host halo at that time. If the
infall happened a number of host halo radii away from the host, we conclude that the particle
was, indeed, part of the subhalo before it became part of the host. This algorithm is perhaps the
most reliable, but has the slight disadvantage that it demands position information in all
<a class="reference internal" href="run_rs_ifl.html"><span class="doc">Infall results</span></a>, i.e., that SPARTA needs to record extra information about particles falling
into halos.</p></li>
<li><p>Gravitationally bound particles. We include the particles within a certain fraction of the
subhalo radius at infall, and require that their gravitational potential (only from the
included particles!) is greater than their kinetic energy relative to the subhalo center by some
factor. The included radius matters greatly; if it is too large, a lot of host material can be
included, resulting in host particles being “bound” to the subhalo. Conversely, if the radius is
too small, there may not be enough gravitational potential to bind almost any particles.</p></li>
</ul>
<p>Once we have determined subhalo membership, we tag the host’s tracer particles with the sub-to-host
mass ratio. However, we require that the host particle fell in less than some factor times the
dynamical time ago in order to avoid tagging particles that somehow were part of both sub and host,
but that had really lived in the host for a long time.</p>
</section>
<section id="subhalo-tracers">
<h2>Subhalo tracers<a class="headerlink" href="#subhalo-tracers" title="Permalink to this headline">¶</a></h2>
<p>Subhalo tracers are treated in much the same way as particles, except that their positions and
velocities are determined by the halo finder. Subhalo tracers are created whenever a new subhalo
is added to a host, but are not deleted if the subhalo leaves the host. Instead, the trajectory
of the (former) subhalo is traced until the halo merges away or the simulation ends. Thus, subhalo
tracers are allowed to stray far away from their previous host, which poses no performance problem
as it does not change the simulation volume for which particles have to be loaded (the halo catalog
is loaded in its entirety anyway).</p>
<p>Subhalos should not be seen as unbiased dynamical tracers such as particles, chiefly because they
suffer from dynamical friction. This is one of the main reasons why it is important to tag
subhalo particles: their orbits have a tendency to move closer to the host halo center. This effect
becomes stronger with sub-to-host mass ratio. Thus, most analyses implemented in SPARTA do not rely
on results from subhalo tracers.</p>
<p>When a subhalo becomes a ghost, we stop tracking it as a subhalo tracer. The reason for this
behavior is mostly technical (ghost’s center and velocity are computed after the subhalo tracers
may be analyzed, meaning they may be undefined at that time). In principle, the tracking of ghost
subhalo tracers could be implemented though.</p>
</section>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Tracers</a><ul>
<li><a class="reference internal" href="#particle-tracers-in-host-halos">Particle tracers in host halos</a></li>
<li><a class="reference internal" href="#particle-tracers-in-subhalos">Particle tracers in subhalos</a></li>
<li><a class="reference internal" href="#which-particles-belong-to-a-subhalo">Which particles belong to a subhalo?</a></li>
<li><a class="reference internal" href="#subhalo-tracers">Subhalo tracers</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="intro_halos.html"
                          title="previous chapter">Halos &amp; Subhalos</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="intro_rs_al.html"
                          title="next chapter">Results &amp; Analyses</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>