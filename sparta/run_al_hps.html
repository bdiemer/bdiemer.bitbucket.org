<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Halo properties analysis &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Analyses" href="run_al.html" >
    <link rel="next" title="Analyzing SPARTA output" href="analysis.html" >
    <link rel="prev" title="Density profile analysis" href="run_al_prf.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="run.html" >Running SPARTA</a></li>
          <li class="active"><a href="run_al.html" accesskey="U">Analyses</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="analysis.html" title="Analyzing SPARTA output"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="run_al_prf.html" title="Density profile analysis"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="halo-properties-analysis">
<h1>Halo properties analysis<a class="headerlink" href="#halo-properties-analysis" title="Permalink to this headline">¶</a></h1>
<p>The halo properties analysis is designed to compute halo properties that depend on the particle
distribution, on tracer properties, and possibly on more advanced data such as pericenter counts.</p>
<p class="rubric">Algorithm for SO masses and radii</p>
<p>The analysis computes SO masses by creating a sorted array of particle radii and comparing the
overdensity enclosed within each particle’s radius to one or multiple density thresholds. Moreover,
the particle distribution may be a sub-selection of all particles. Thus, there are a number of
fundamentally different SO masses:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">all</span></code>: All particles. These masses correspond to a strict overdensity criterion for both host
and subhalos. For subhalos, strict SO masses may not be very sensible as they likely include a
large contribution from the host mass. In such cases, the mass at infall or peak is often used
instead of the instantaneous subhalo mass. For all-particle masses, we do not tolerate cases
where the density never decreases to the threshold; for such halos, we set an error code.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">bnd</span></code>: Gravitationally bound particles. Here, the distribution of particles is taken inside a
certain radius determined through config parameters (see below). Then, that distribution is
subjected to (possibly iterative) unbinding, where a particle is considered unbound if its
kinetic energy with respect to the halo center is larger than some factor times its gravitational
binding energy (computed approximately by using a tree potential). Note that the result is
strongly dependent on the radius within which particles are considered: if that radius is very
large, e.g., multiple times R200m, then almost all particles will be bound. If the radius is
small, no particles may be bound, in which case a halo mass and radius of zero are output.
Generally, one should not expect that the bound masses match bound masses from a halo finder.
For example, Rockstar uses a friends-of-friends group as the initial halo membership, which
cannot be reproduced with any SO definition. For bound masses, we tolerate SO masses where the
density never decreases to the threshold because the bound particle distribution can be very
compact. In that case, multiple SO definitions can agree on the same mass (but different radii).</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">tcr</span></code>: Tracer masses. This definition applies only to subhalos when sub-particle tracking is
on (see <a class="reference internal" href="intro_tracers.html"><span class="doc">Tracers</span></a>). At subhalo infall, particles are tagged if they are deemed to
physically belong to the subhalo. The tracer mass is then defined as an SO mass only for those
particles, while no new particles are added to the subhalo (because they are thought to likely
belong to the host). For host halos, the all-particle mass is substituted for completeness.
As for bound masses, we tolerate cases where the density never decreases to the threshold (which
is a fairly common case for tracer masses).</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">orb</span></code>: Orbiting particles, namely particles that have undergone at least one pericenter as
determined by their <a class="reference internal" href="run_rs_oct.html"><span class="doc">Orbit count results</span></a> tracer results. This definition applies for both hosts
and subhalos.</p></li>
</ul>
<p>Thus, valid definitions include <code class="docutils literal notranslate"><span class="pre">R200m_all</span></code>, <code class="docutils literal notranslate"><span class="pre">R500c_all</span></code>, <code class="docutils literal notranslate"><span class="pre">M500c_all</span></code>, <code class="docutils literal notranslate"><span class="pre">M200c_tcr</span></code>,
<code class="docutils literal notranslate"><span class="pre">M200c_bnd</span></code>, <code class="docutils literal notranslate"><span class="pre">R200m_orb</span></code> or any other combination of those elements.</p>
<p class="rubric">Radii and masses from percentiles of orbiting particles</p>
<p>Unlike SO definitions, these experimental definitions are based on the distribution of orbiting
particles but do not rely on an overdensity threshold. There are two fundamental types of
definition:</p>
<ul class="simple">
<li><p><code class="docutils literal notranslate"><span class="pre">Morb-all</span></code>: The total mass of all particles that have ever (!) had a pericenter in this halo.
These particles can be at arbitrary distances at the current time.</p></li>
<li><p><code class="docutils literal notranslate"><span class="pre">Morb-p&lt;percentile&gt;</span></code> and <code class="docutils literal notranslate"><span class="pre">Rorb-p&lt;percentile&gt;</span></code>: these definitions pick out the radius where a
particular percentile of the orbiting particles is reached. Since we need the radii of the
orbiting particles, we cannot include particles at arbitrary distances. Instead, only particles
within <code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_orb_host</span></code> or <code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_orb_sub</span></code> times R200m of the halo are
considered. If those radii are large enough (e.g., at least 3R200m), the vast majority of
orbiting particles is typically included. This can be checked by comparing <code class="docutils literal notranslate"><span class="pre">Morb-p99</span></code> to
<code class="docutils literal notranslate"><span class="pre">Morb-all</span></code>, for example. The radius is linearly interpolated between neighboring particles.
By construction, the percentile masses are trivially related to each other, but the radii depend
on the radial profile of orbiting particles.</p></li>
</ul>
<p>Note that all orbiting definitions depend critically on the algorithm to determine pericenters.
Thus, they are to be seen as experimental.</p>
<p class="rubric">Compile-time parameters</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 33%" />
<col style="width: 67%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Parameter</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_HALOPROPS</span></code></p></td>
<td><p>Write halo properties analyses to output file</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_HALOPROPS_RM</span></code></p></td>
<td><p>Compute spherical overdensity radii and masses for definitions selected by the user</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_ANALYSIS_HALOPROPS_ORBITING</span></code></p></td>
<td><p>Compute SO radii and masses that include only orbiting particles (triggers OCT results)</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">ANALYSIS_HALOPROPS_MAX_SNAPS</span></code></p></td>
<td><p>The maximum number of snapshots that can be requested by the user</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">ANALYSIS_HALOPROPS_MAX_DEFINITIONS</span></code></p></td>
<td><p>The maximum number of definitions that can be requested by the user</p></td>
</tr>
</tbody>
</table>
<p>If memory is an issue, the <code class="docutils literal notranslate"><span class="pre">ANALYSIS_HALOPROPS_MAX_SNAPS</span></code> and <code class="docutils literal notranslate"><span class="pre">ANALYSIS_HALOPROPS_MAX_DEFINITIONS</span></code>
parameters should be adjusted close to the values they must have to accommodate a given simulation
and user preferences.</p>
<p class="rubric">Run-time parameters</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 24%" />
<col style="width: 6%" />
<col style="width: 9%" />
<col style="width: 61%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Parameter</p></th>
<th class="head"><p>Type</p></th>
<th class="head"><p>Default</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_hps_redshifts</span></code></p></td>
<td><p>[float]</p></td>
<td><p>-1</p></td>
<td><p>Redshifts where halo properties are output; list of floats, or -1 for all</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_hps_defs</span></code></p></td>
<td><p>list</p></td>
<td><p>None</p></td>
<td><p>Spherical overdensity definitions to be computed</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_so_host</span></code></p></td>
<td><p>float</p></td>
<td><p>2.0</p></td>
<td><p>Maximum radius within which particles are considered for hosts, in units of R200m</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_sub</span></code></p></td>
<td><p>float</p></td>
<td><p>2.0</p></td>
<td><p>Maximum radius within which particles are considered for subs, in units of R200m</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_orb_host</span></code></p></td>
<td><p>float</p></td>
<td><p>3.0</p></td>
<td><p>Maximum radius within which orbiting ptls. are considered for hosts, in units of R200m</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_orb_sub</span></code></p></td>
<td><p>float</p></td>
<td><p>3.0</p></td>
<td><p>Maximum radius within which orbiting ptls. are considered for subs, in units of R200m</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_hps_r_unbinding_host</span></code></p></td>
<td><p>float</p></td>
<td><p>1.0</p></td>
<td><p>Radius within which host particles are considered for unbinding, in units of R200m</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">anl_hps_r_unbinding_sub</span></code></p></td>
<td><p>float</p></td>
<td><p>1.0</p></td>
<td><p>Rad. within which subhalo ptl are considered for unbinding, in units of R200m at infall</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">anl_hps_iterative_unbinding</span></code></p></td>
<td><p>bool</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">FALSE</span></code></p></td>
<td><p>Unbind once or iteratively, taking new halo membership into account</p></td>
</tr>
</tbody>
</table>
<p>The convention for choosing mass and radius definitions is described in
<a class="reference internal" href="intro_conventions_halodef.html"><span class="doc">Halo radius and mass definitions</span></a>.</p>
<p>Note that choosing redshifts using the <code class="docutils literal notranslate"><span class="pre">anl_hps_redshifts</span></code> parameter saves memory and disk space,
but can lead to issues when creating halo catalogs with MORIA. If in doubt, it is probably best to
output the analysis for all snapshots.</p>
<p>The <code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_so_host</span></code> and <code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_sub</span></code> parameters give the maximum radius to
which all-particle SO radii are allowed. These radii do contribute to the particle search radius,
that is, we guarantee that all particles are available within those radii. Thus, if they are set
to a value larger than other radii (such as tracer radii), they may slow down the code.</p>
<p>However, for hosts, the <code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_so_host</span></code> parameter can safely be left at a value near
1 as long as no definition with a larger radius than R200m is chosen. If a larger radius is
chosen, e.g., 180m, then this radius can be slightly larger than R200m at high redshift (note that
Rvir approaches R178m at high redshift). Importantly, making <code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_so_host</span></code> or
<code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_sub</span></code> larger does not affect run time if there are other, larger radii already
forcing the code to look for particles further out. Conversely, we do not use any particles
beyond those radii even if they are available, for example for density profiles, in order to
prevent any dependence on extraneous factors.</p>
<p>If the threshold cannot be reached within the search radius, i.e., if the density is higher than
the threshold at all considered radii, the mass/radius cannot be computed and the status field
will indicate that (see below). When the search radius is near 200m, some small (typically less
than 100 particle) halos may not be assigned a valid 200m by this analysis due to numerical
reasons. If you care about such halos, please increase <code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_so_host</span></code>.</p>
<p>The corresponding parameter for subhalos, <code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_sub</span></code>, sets the search radius in units
of R200m at infall. Note that SO radii are generally not well-defined for subhalos due to the
contribution of mass from the host. Thus, it is not recommended to increase <code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_sub</span></code>
to large values only to get SO radii that are more or less meaningless anyway, unless that is
the specific purpose of the calculation. We do not output radii/masses if the threshold was not
reached as they are physically meaningless and will totally depend on <code class="docutils literal notranslate"><span class="pre">anl_hps_r_max_sub</span></code>.</p>
<p>For <code class="docutils literal notranslate"><span class="pre">tcr</span></code> masses, however, we do allow radii/masses where the outer threshold was not reached,
as we consider their particle set to be finite (unlike the overall particle distribution). Thus,
their total mass is well-defined even if they never formally reach a threshold.</p>
<p>If <code class="docutils literal notranslate"><span class="pre">anl_hps_iterative_unbinding</span></code> is on, the unbinding procedure (for <code class="docutils literal notranslate"><span class="pre">_bnd</span></code> masses) is
performed iteratively, meaning that the potential is recomputed without particles that have
previously been unbound. This procedure should, in principle, lead to more accurate results, but
can also be very time-consuming. In practice the results are not different enough to warrant the
computational cost. See also the <code class="docutils literal notranslate"><span class="pre">potential_err_tol</span></code> parameter in the general configuration
(<a class="reference internal" href="run_config.html"><span class="doc">Run-time configuration parameters</span></a>).</p>
<p class="rubric">Output fields</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 13%" />
<col style="width: 5%" />
<col style="width: 25%" />
<col style="width: 18%" />
<col style="width: 40%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Field</p></th>
<th class="head"><p>Type</p></th>
<th class="head"><p>Dimensions</p></th>
<th class="head"><p>Exists if</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">halo_first</span></code></p></td>
<td><p>int64</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_halos</span></code></p></td>
<td><p>Always</p></td>
<td><p>The index of the first analysis for each halo (or -1 if none exists for a halo).</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">halo_n</span></code></p></td>
<td><p>int32</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_halos</span></code></p></td>
<td><p>Always</p></td>
<td><p>The number of analyses of this type for each halo (can be 0).</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">halo_id</span></code></p></td>
<td><p>int63</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_al_haloprops</span></code></p></td>
<td><p>Always</p></td>
<td><p>The (original, first-snapshot) halo ID to which this analysis refers.</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">&lt;halo</span> <span class="pre">defn&gt;</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_al_haloprops</span></code> * <code class="docutils literal notranslate"><span class="pre">n_snaps</span></code></p></td>
<td><p>Always</p></td>
<td><p>One field for each halo definition (see above)</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">status_&lt;halo</span> <span class="pre">defn&gt;</span></code></p></td>
<td><p>int8</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_al_haloprops</span></code> * <code class="docutils literal notranslate"><span class="pre">n_snaps</span></code></p></td>
<td><p>Always</p></td>
<td><p>Status for each definition and redshift</p></td>
</tr>
</tbody>
</table>
<p>Here, <code class="docutils literal notranslate"><span class="pre">n_snaps</span></code> is, of course, the number of redshifts chosen by the user of the number of
snapshots in the simulation if <code class="docutils literal notranslate"><span class="pre">anl_rsp_redshifts</span></code> is -1. The status field can take on the
following values:</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 5%" />
<col style="width: 26%" />
<col style="width: 70%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Value</p></th>
<th class="head"><p>Parameter</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p>0</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_HPS_STATUS_UNDEFINED</span></code></p></td>
<td><p>Placeholder, should never occur in output file</p></td>
</tr>
<tr class="row-odd"><td><p>1</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_HPS_STATUS_SUCCESS</span></code></p></td>
<td><p>The analysis succeeded, all output values can be used</p></td>
</tr>
<tr class="row-even"><td><p>2</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_HPS_STATUS_HALO_NOT_VALID</span></code></p></td>
<td><p>Halo could not be analyzed at this snapshot, e.g. because it didn’t exist</p></td>
</tr>
<tr class="row-odd"><td><p>3</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_HPS_STATUS_HALO_NOT_SAVED</span></code></p></td>
<td><p>Halo was not saved to the SPARTA output file at all (used in MORIA)</p></td>
</tr>
<tr class="row-even"><td><p>4</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_HPS_STATUS_NOT_FOUND</span></code></p></td>
<td><p>Analysis not found for this halo (used in MORIA)</p></td>
</tr>
<tr class="row-odd"><td><p>5</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_HPS_STATUS_SO_TOO_SMALL</span></code></p></td>
<td><p>The density was lower than the SO threshold everywhere within the search radius</p></td>
</tr>
<tr class="row-even"><td><p>6</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_HPS_STATUS_SO_TOO_LARGE</span></code></p></td>
<td><p>The density was higher than the SO threshold everywhere within the search radius</p></td>
</tr>
<tr class="row-odd"><td><p>7</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">ANL_HPS_STATUS_ORB_ZERO</span></code></p></td>
<td><p>There were no orbiting particles</p></td>
</tr>
</tbody>
</table>
<p>The <code class="docutils literal notranslate"><span class="pre">ANL_HPS_STATUS_SO_TOO_LARGE</span></code> status occurs frequently for subhalos, where an SO
boundary can often not be obtained because the mass profile is dominated by the host halo.</p>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="run_al_prf.html"
                          title="previous chapter">Density profile analysis</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="analysis.html"
                          title="next chapter">Analyzing SPARTA output</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>