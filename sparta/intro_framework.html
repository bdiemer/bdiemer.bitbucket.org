<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Code framework &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Introduction" href="intro.html" >
    <link rel="next" title="Units and conventions" href="intro_conventions.html" >
    <link rel="prev" title="Results &amp; Analyses" href="intro_rs_al.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="intro.html" accesskey="U">Introduction</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="intro_conventions.html" title="Units and conventions"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="intro_rs_al.html" title="Results &amp; Analyses"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="code-framework">
<h1>Code framework<a class="headerlink" href="#code-framework" title="Permalink to this headline">¶</a></h1>
<p>While SPARTA is a fairly general framework, it does impose a particular workflow (i.e., order of
operations) and a particular, although flexible, memory structure. The information on this page is
not strictly speaking necessary in order to run SPARTA, but it will make it easier to understand
how the individual modules work together.</p>
<section id="workflow">
<h2>Workflow<a class="headerlink" href="#workflow" title="Permalink to this headline">¶</a></h2>
<p>SPARTA goes through a simulation in a time-forward manner. After some small preliminary tasks, the
main effort is in going through each snapshot of the simulation (or a subset selected by the user).
The following flow chart shows this workflow.</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<a class="reference internal image-reference" href="_images/sparta_flowchart.jpeg"><img alt="_images/sparta_flowchart.jpeg" class="align-center" src="_images/sparta_flowchart.jpeg" style="width: 512.0px; height: 384.0px;" /></a>
<div class="line-block">
<div class="line"><br /></div>
</div>
<p>The tasks are split into a hierarchy with three levels, indicated by the different colors. They
correspond to:</p>
<ul class="simple">
<li><p>Red: Tasks that demand communication between processes. These tasks are often guided by the main
process, which collects additional information from the workers. For example, all processes
request the descendants of their halos from the main process, which then reads the halo
catalog and distributes the information. Similarly, the output file is written solely by the
main process.</p></li>
<li><p>Blue: Tasks that can be performed separately on each worker process, but that refer to the entire
set of halos on that process. For example, we build one particle tree that is used to search for
particles in all halos on that process. Similarly, the assignment of subhalos to their hosts
demands knowledge about multiple halos at once.</p></li>
<li><p>Yellow: Tasks that refer to only one halo. Once the halos have been correctly initialized with
their status and other information, the halo work does not make reference to other halos.</p></li>
</ul>
<p>Note that results are saved to the HDF5 output file on a continuous basis. In particular, results
are written per halo when a halo ends.</p>
</section>
<section id="domain-decomposition">
<h2>Domain decomposition<a class="headerlink" href="#domain-decomposition" title="Permalink to this headline">¶</a></h2>
<p>SPARTA is a fully MPI-parallelized code. The domain decomposition is performed over halos, meaning
that each halo lives on one process. Whenever a new halo with no progenitor is found in the halo
catalog, a new object is created and sent to the process that is responsible for its location in
space. Subhalos are forced to live on the same process as their host. Two different domain
decomposition schemes are available:</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<a class="reference internal image-reference" href="_images/sparta_domain_decomposition.png"><img alt="_images/sparta_domain_decomposition.png" class="align-center" src="_images/sparta_domain_decomposition.png" style="width: 614.4px; height: 460.79999999999995px;" /></a>
<div class="line-block">
<div class="line"><br /></div>
</div>
<p>If the slab decomposition is chosen, the domain is divided into a series of slabs in each dimension,
and the slab boundaries are slowly adjusted to balance out the work load. This scheme is fast and
works well when the halos are distributed relatively evenly throughout the domain. The number of
cores must be the multiple of three integers in the three dimensions.</p>
<p>The space-filling curve (SFC) decomposition uses a Peano-Hilbert curve to divide space. Halos
centers are snapped to the nearest point in the curve as shown in the schematic above. The curve
can then be divided into an arbitrary number of snippets. The advantage of this scheme is that
the number of processes is arbitrary and that one process can obtain an arbitrarily small
fraction of the SFC, for example, one very massive halo.</p>
<p>At each snapshot, each process computes the boundaries of the rectilinear,
potentially periodic volume that contains all its halos, including a particular search radius
around the halo centers.
This radius depends on the tracers in each halo and various settings. All particles contained
within the rectilinear volume are loaded from snapshot files, and a tree is constructed from their
positions (we use the tree implementation of ROCKSTAR). For each halo, the particles within its
search radius are found using a tree search.</p>
</section>
<section id="memory-structure">
<h2>Memory structure<a class="headerlink" href="#memory-structure" title="Permalink to this headline">¶</a></h2>
<p>The following chart shows the basic memory structure of SPARTA. The memory is organized on a
per-halo basis because a halo has to always live on one process. When halos are exchanged between
processes, all their dynamically allocated memory is also transferred. All large fields, such as
the lists of tracer, result, and analysis objects, are dynamically allocated.</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<a class="reference internal image-reference" href="_images/sparta_memory.jpeg"><img alt="_images/sparta_memory.jpeg" class="align-center" src="_images/sparta_memory.jpeg" style="width: 480.0px; height: 309.0px;" /></a>
<div class="line-block">
<div class="line"><br /></div>
</div>
<p>To avoid memory leaks, SPARTA uses an internal memory control system that detects even the
smallest leaks and warns the user (though that should obviously never happen).</p>
</section>
<section id="restarting">
<h2>Restarting<a class="headerlink" href="#restarting" title="Permalink to this headline">¶</a></h2>
<p>When running on large simulations, SPARTA’s runtime can be significant, meaning that a crash at a
late-time snapshot can be time-consuming to debug. For example, the memory consumption tends to
increase with time as more and more halos are formed and as they grow, so that out-of-memory errors
are likely to happen towards the end of the simulation. The code will then need to be run again
with more memory per core.</p>
<p>To facilitate such changes and debugging, SPARTA offers full restarting capabilities. Binary
restart files contain an image of the entire allocated memory on each process, as well as the
current state of the output file. The user can decide how frequently restart files are written
to disk. When restarting, SPARTA will continue from the most recent set of restart files as if
nothing had happened.</p>
<p>During a restart, the basic configuration of the code cannot be changed, that is, the results and
analyses that are enabled must stay the same. Similarly, the run-time configuration cannot be
changed and the number of cores must remain the same. However, the but the code can be recompiled
to fix bugs and/or turn on debug options, and the code can be run with a higher memory allocation.</p>
<p>Since restart files can become large, SPARTA automatically deletes old restart files when writing
new ones.</p>
</section>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h3><a href="index.html">Table of Contents</a></h3>
    <ul>
<li><a class="reference internal" href="#">Code framework</a><ul>
<li><a class="reference internal" href="#workflow">Workflow</a></li>
<li><a class="reference internal" href="#domain-decomposition">Domain decomposition</a></li>
<li><a class="reference internal" href="#memory-structure">Memory structure</a></li>
<li><a class="reference internal" href="#restarting">Restarting</a></li>
</ul>
</li>
</ul>

  </div>
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="intro_rs_al.html"
                          title="previous chapter">Results &amp; Analyses</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="intro_conventions.html"
                          title="next chapter">Units and conventions</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>