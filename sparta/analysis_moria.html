<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Creating halo catalogs with MORIA &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Analyzing SPARTA output" href="analysis.html" >
    <link rel="next" title="Configuring MORIA" href="analysis_moria_config.html" >
    <link rel="prev" title="General utilities" href="analysis_python_utils.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="analysis.html" accesskey="U">Analyzing SPARTA output</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="analysis_moria_config.html" title="Configuring MORIA"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="analysis_python_utils.html" title="General utilities"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="creating-halo-catalogs-with-moria">
<h1>Creating halo catalogs with MORIA<a class="headerlink" href="#creating-halo-catalogs-with-moria" title="Permalink to this headline">¶</a></h1>
<p>MORIA is an extension of SPARTA that post-processes output files to create user-defined halo
catalogs. The name refers to sacred, publicly owned olive trees in ancient Greece, hinting at
MORIA’s dealing with merger trees.</p>
<p class="rubric">Basics</p>
<p>SPARTA stores its results in a single HDF5 file on a per-halo basis, where halos are defined as in
the original, input halo catalog. For many applications, however, we wish to create an altered halo
catalog, for example, one containing splashback radii and masses. The purpose of MORIA is to
create such catalogs in post-processing, by combining the input catalogs with SPARTA output:</p>
<div class="line-block">
<div class="line"><br /></div>
</div>
<a class="reference internal image-reference" href="_images/sparta_schematic.jpeg"><img alt="_images/sparta_schematic.jpeg" class="align-center" src="_images/sparta_schematic.jpeg" style="width: 512.0px; height: 208.0px;" /></a>
<div class="line-block">
<div class="line"><br /></div>
</div>
<p>There are a number of reasons why MORIA is a separate tool and why it is run in post-processing,
most importantly:</p>
<ul class="simple">
<li><p>SPARTA can take hours to run on many cores, whereas MORIA typically runs quickly and on one
process only.</p></li>
<li><p>The output from SPARTA can be used for many different purposes, and be used in many different
halo catalogs. MORIA allows the user to easily create such catalogs without re-running SPARTA.</p></li>
<li><p>MORIA is entirely configured at run-time, meaning it does not need to be re-compiled for
different operations in the way SPARTA does.</p></li>
</ul>
<p>MORIA is, however, written in pure C in order to deal with very large catalog files. Some of the
most important features are:</p>
<ul class="simple">
<li><p>MORIA works snapshot by snapshot, the user can choose which redshifts halo catalogs should be
created for.</p></li>
<li><p>MORIA tries to improve the completeness of catalogs, for example, by approximating SPARTA
results such as splashback radii where they could not be computed by SPARTA.</p></li>
<li><p>An important part of creating the catalogs is to compute host-sub relations for a variety of
user-defined halo radius definitions, including spherical overdensity and splashback radii.</p></li>
<li><p>MORIA can output both a native HDF5 catalog format as well as the format of the input halo
finder.</p></li>
<li><p>MORIA can output a HDF5-based tree format where all halos and snapshots are combined into one
file.</p></li>
</ul>
<p class="rubric">How are host-subhalo relations computed?</p>
<p>One of the main tasks of MORIA is to compute host-subhalo relations for a number of definitions
requested by the user. The following schematic highlights a few situations:</p>
<a class="reference internal image-reference" href="_images/moria_hostsub.jpeg"><img alt="_images/moria_hostsub.jpeg" class="align-center" src="_images/moria_hostsub.jpeg" style="width: 512.0px; height: 384.0px;" /></a>
<p>Each assignment is output in a <code class="docutils literal notranslate"><span class="pre">parent_id_&lt;def&gt;</span></code> field. A parent ID (PID) of <code class="docutils literal notranslate"><span class="pre">-1</span></code> indicates
that the halo is a host. If a halo is the sub of a subhalo but outside other, larger halos, it
is still counted as a subhalo (halo 5 above). In this case, we can assign it the ID of its direct
parent (the default behavior) or the ID of the host’s host (if <code class="docutils literal notranslate"><span class="pre">hostsub_assign_subparent_id</span></code> is
set). If a halo is a sub of a subhalo but inside the host’s host, it is always assigned the primary
host’s ID (1 in the case of halo 3 above). In Rockstar, these options are distinguished by the
<code class="docutils literal notranslate"><span class="pre">id</span></code> and <code class="docutils literal notranslate"><span class="pre">upid</span></code> fields, in MORIA we always assign the equivalent of <code class="docutils literal notranslate"><span class="pre">upid</span></code>.</p>
<p>The figure also illustrates a case where the smaller halo (according to the radius definition in
question) is host to a larger subhalo (IDs 8 and 9). The reason for this assignment is that
the smaller halo has the larger mass in the definition used to order the halos (<code class="docutils literal notranslate"><span class="pre">order_def</span></code>),
in this example <span class="math notranslate nohighlight">\(V_{\rm max}\)</span>.</p>
<p>To establish host-sub relations, each halo must have some estimate of a radius for each definition,
which causes trouble in cases where no such radius is available. MORIA solves such cases as
follows:</p>
<ul class="simple">
<li><p>For splashback radii, a radius is always computed, reconstructed, or guessed (see above).</p></li>
<li><p>For SO radii from the catalog, the radius can be zero, for example in cases where the density
never reaches a particular threshold. We keep the zero radius, meaning that such halos are
deemed too small to host subhalos.</p></li>
<li><p>For halos where the <code class="docutils literal notranslate"><span class="pre">SO_TOO_SMALL</span></code> status was set by SPARTA, we similarly set a radius to
zero as the real radius is too small to be resolved.</p></li>
<li><p>For halos where the <code class="docutils literal notranslate"><span class="pre">SO_TOO_LARGE</span></code> status was set by SPARTA, the situation is more
complicated. Most of those halos are subhalos where the profile included the host halo, and thus
never went down to the desired density threshold. This leaves us with no real information about
the halo’s “true” size. We thus again set the radius to zero, as the subhalo is likely close
to the host’s center anyway.</p></li>
<li><p>Depending on the chosen cut threshold for halos, some halos may not be found in the SPARTA
file. This indicates a poor matching of the lower mass cutoff in SPARTA and MORIA, since the
MORIA cutoff should ideally be more conservative. However, when setting a threshold that is
not simply as mass such as Vmax, some halos may not have had sufficient mass to be output by
SPARTA even though they make a reasonably conservative Vmax cut. Again, we set their radius to
zero; this is not physical, and the user is encouraged to always check that the vast majority of
halos in a MORIA output catalog should have been found by SPARTA
(see <a class="reference internal" href="analysis_moria_exec.html"><span class="doc">Compiling and Running MORIA</span></a>).</p></li>
</ul>
<p>In practice, as long as not too many halos are missing in SPARTA, the impact on the overall
statistics of the host-sub assignments should be small.</p>
<p>One final note: the host-sub assignments in MORIA use all halos in the catalog, regardless of the
mass (or other) cut the user has imposed. Thus, it is possible that some halos are subhalos of
halos that are not written to the final catalog (or tree) files. This situation can be avoided if
the ordering and cut quantities are the same (see <a class="reference internal" href="analysis_moria_config.html"><span class="doc">Configuring MORIA</span></a>).</p>
<p class="rubric">Contents</p>
<div class="toctree-wrapper compound">
<ul>
<li class="toctree-l1"><a class="reference internal" href="analysis_moria_config.html">Configuring MORIA</a></li>
<li class="toctree-l1"><a class="reference internal" href="analysis_moria_exec.html">Compiling and Running MORIA</a></li>
</ul>
</div>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="analysis_python_utils.html"
                          title="previous chapter">General utilities</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="analysis_moria_config.html"
                          title="next chapter">Configuring MORIA</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>