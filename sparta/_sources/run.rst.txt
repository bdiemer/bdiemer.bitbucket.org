***************************************************************************************************
Running SPARTA
***************************************************************************************************

This section contains instructions for how to configure and run SPARTA. Typically, SPARTA needs to 
be configured both at compile-time (:doc:`run_compile_config`) and at run-time (:doc:`run_config`): the
basic operations are chosen before compiling, and more detailed parameters are set at run-time.

This section also gives details about many of the operations SPARTA can perform, namely 
:doc:`run_rs` and :doc:`run_al`.

.. rubric:: Contents

.. toctree::
    :maxdepth: 2

    run_compile
    run_compile_config
    run_config
    run_exec
    run_rs
    run_al
    