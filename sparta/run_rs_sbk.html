<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    <meta name="generator" content="Docutils 0.17.1: http://docutils.sourceforge.net/" />

    <title>Splashback results &mdash; Sparta Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '1.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/documentation_options.js"></script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="Sparta Documentation" href="index.html" >
    <link rel="up" title="Results" href="run_rs.html" >
    <link rel="next" title="Trajectory results" href="run_rs_tjy.html" >
    <link rel="prev" title="Infall results" href="run_rs_ifl.html" > 
  </head>
  <body>

<div class="container">
  <div class="top-scipy-org-logo-header">
    <a href="index.html">
      <img style="border: 0;" alt="SciPy" src="_static/img/scipy_org_logo.gif"></a>
    </div>
  </div>
</div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/sparta">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">Sparta Documentation</a></li>
	
          <li class="active"><a href="run.html" >Running SPARTA</a></li>
          <li class="active"><a href="run_rs.html" accesskey="U">Results</a></li> 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="run_rs_tjy.html" title="Trajectory results"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="run_rs_ifl.html" title="Infall results"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <section id="splashback-results">
<h1>Splashback results<a class="headerlink" href="#splashback-results" title="Permalink to this headline">¶</a></h1>
<p>The splashback result routines measure the time and location of a tracer’s first apocenter after
infall into a halo. The main point of the splashback results is that they can be used in the
<a class="reference internal" href="run_al_rsp.html"><span class="doc">Splashback radius analysis</span></a> to determine a halo’s slashback radius and mass.</p>
<p class="rubric">Algorithm</p>
<p>The splashback measurement can be somewhat tricky because we can only keep a small
fraction of a trajectory in memory, and because the trajectories may be noisy. For simplicity, we
ignore all angular information and consider only the tracer’s radius and radial velocity. We
consider the four last snapshots in time.</p>
<p>Before we can look for apocenter, we need to establish pericenter, i.e., the first passage of the
tracer through the host halo. This event is much clearer in the radial velocity than in radius, as
the radial velocity must switch from negative to positive. We look for this upwards crossing of
zero velocity and require that it is robust in the sense that the first two time bins have negative
velocity and the second two positive velocity. If the trajectory is ambiguous, for example if v
becomes positive for only one snapshot, we record the number of such invalid switches. If the
number exceeds one, we abort the trajectory as it is likely under-sampled in time (that is, the
orbital time is shorter than the snapshot spacing). When we find a pericener, we interpolate the
radial trajectory in time to find the radius of closest approach.</p>
<p>Once we have found a pericenter, we begin looking for an apocenter. Again, we search for a zero-
crossing in the radial velocity, but this time from positive to negative. We again require
the first two bins to have positive v and the second two negative, otherwise we abort. We then
linearly interpolate the trajectory to find the time and radius of the apocenter.</p>
<p>Finally, we compute the mass enclosed within the splashback event by interpolating the halo’s mass
profile in time and radius. This algorithm identifies a splashback event for about 90% of the
particle trajectories, about 85% of subhalo trajectories (see
<a class="reference external" href="https://ui.adsabs.harvard.edu//#abs/2017ApJS..231....5D/abstract">Diemer 2017</a> for details).</p>
<p class="rubric">Compile-time parameters</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 33%" />
<col style="width: 67%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Parameter</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_SPLASHBACK</span></code></p></td>
<td><p>Write splashback results to output file</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_SPLASHBACK_MSP</span></code></p></td>
<td><p>Save the splashback mass as well as the radius in output file</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_SPLASHBACK_RRM</span></code></p></td>
<td><p>Save the pericenter radius (closest approach to halo center in units of R200m) of the tracer</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_SPLASHBACK_TMIN</span></code></p></td>
<td><p>Save the pericenter time (closest approach to halo center)</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_SPLASHBACK_POS</span></code></p></td>
<td><p>Save the 3-dimensional coordinates (rather than just radius) of the splashback event in output file</p></td>
</tr>
</tbody>
</table>
<p class="rubric">Run-time parameters</p>
<p>This result does not add any config parameters.</p>
<p class="rubric">Output fields</p>
<table class="docutils align-default">
<colgroup>
<col style="width: 14%" />
<col style="width: 5%" />
<col style="width: 15%" />
<col style="width: 19%" />
<col style="width: 46%" />
</colgroup>
<thead>
<tr class="row-odd"><th class="head"><p>Field</p></th>
<th class="head"><p>Type</p></th>
<th class="head"><p>Dimensions</p></th>
<th class="head"><p>Exists if</p></th>
<th class="head"><p>Explanation</p></th>
</tr>
</thead>
<tbody>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">halo_first</span></code></p></td>
<td><p>int64</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_halos</span></code></p></td>
<td><p>Always</p></td>
<td><p>The index of the first result for each halo (or -1 if none exists for a halo).</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">halo_n</span></code></p></td>
<td><p>int32</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_halos</span></code></p></td>
<td><p>Always</p></td>
<td><p>The number of results of this type for each halo (can be 0).</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">tracer_id</span></code></p></td>
<td><p>int64</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_splashback</span></code></p></td>
<td><p>Always</p></td>
<td><p>The ID of the tracer to which this result refers.</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">tsp</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_splashback</span></code></p></td>
<td><p>Always</p></td>
<td><p>The time of splashback in Gyr since the Big Bang</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">rsp</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_splashback</span></code></p></td>
<td><p>Always</p></td>
<td><p>The apocenter (splashback) radius of the tracer in physical kpc/h</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">msp</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_splashback</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_SPLASHBACK_MSP</span></code></p></td>
<td><p>The mass enclosed within the apocenter radius in Msun/h</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">rrm</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_splashback</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_SPLASHBACK_RRM</span></code></p></td>
<td><p>rmin/R200(tmin), the radius at pericenter in units of R200m</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">tmin</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_splashback</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_SPLASHBACK_TMIN</span></code></p></td>
<td><p>tmin, the time at pericenter</p></td>
</tr>
<tr class="row-even"><td><p><code class="docutils literal notranslate"><span class="pre">theta</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_splashback</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_SPLASHBACK_POS</span></code></p></td>
<td><p>First angular coordinate of splashback event (see below)</p></td>
</tr>
<tr class="row-odd"><td><p><code class="docutils literal notranslate"><span class="pre">phi</span></code></p></td>
<td><p>float</p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">n_rs_splashback</span></code></p></td>
<td><p><code class="docutils literal notranslate"><span class="pre">OUTPUT_RESULT_SPLASHBACK_POS</span></code></p></td>
<td><p>Second angular coordinate of splashback event (see below)</p></td>
</tr>
</tbody>
</table>
<p>The polar coordinates <code class="docutils literal notranslate"><span class="pre">theta</span></code> and <code class="docutils literal notranslate"><span class="pre">phi</span></code> are given in the convention where</p>
<div class="math notranslate nohighlight">
\[\begin{split}x = r \times sin(\theta) cos(\phi) \\
y = r \times sin(\theta) sin(\phi) \\
z = r \times cos(\theta)\end{split}\]</div>
</section>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <div>
    <h4>Previous topic</h4>
    <p class="topless"><a href="run_rs_ifl.html"
                          title="previous chapter">Infall results</a></p>
  </div>
  <div>
    <h4>Next topic</h4>
    <p class="topless"><a href="run_rs_tjy.html"
                          title="next chapter">Trajectory results</a></p>
  </div>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2023, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 4.5.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>