===============================
Springel & Hernquist 2003 Model
===============================

.. automodule:: models.model_sh03
    :members:
    