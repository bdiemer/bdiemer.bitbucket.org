<!DOCTYPE html>

<html>
  <head>
    <meta charset="utf-8">
    
    <title>Model Base Class &mdash; ISM Model Documentation</title>
    
    <link rel="stylesheet" type="text/css" href="_static/css/spc-bootstrap.css">
    <link rel="stylesheet" type="text/css" href="_static/css/spc-extend.css">
    <link rel="stylesheet" href="_static/scipy.css" type="text/css" >
    <link rel="stylesheet" href="_static/pygments.css" type="text/css" >
    
    <script type="text/javascript">
      var DOCUMENTATION_OPTIONS = {
        URL_ROOT:    './',
        VERSION:     '0.1.0',
        COLLAPSE_INDEX: false,
        FILE_SUFFIX: '.html',
        HAS_SOURCE:  true
      };
    </script>
    <script type="text/javascript" src="_static/jquery.js"></script>
    <script type="text/javascript" src="_static/underscore.js"></script>
    <script type="text/javascript" src="_static/doctools.js"></script>
    <script type="text/javascript" src="_static/language_data.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.5/latest.js?config=TeX-AMS-MML_HTMLorMML"></script>
    <script type="text/javascript" src="_static/js/copybutton.js"></script>
    <link rel="index" title="Index" href="genindex.html" >
    <link rel="search" title="Search" href="search.html" >
    <link rel="top" title="ISM Model Documentation" href="index.html" >
    <link rel="next" title="Testing model implementations" href="test_models.html" >
    <link rel="prev" title="Units" href="units.html" > 
  </head>
  <body>

  <div class="container">
    <div class="header">
    </div>
  </div>


    <div class="container">
      <div class="main">
        
	<div class="row-fluid">
	  <div class="span12">
	    <div class="spc-navbar">
              
    <ul class="nav nav-pills pull-left">
        <li class="active"><a href="https://bitbucket.org/bdiemer/ismmodel">BitBucket Repository</a></li>
	
        <li class="active"><a href="index.html">ISM Model Documentation</a></li>
	 
    </ul>
              
              
    <ul class="nav nav-pills pull-right">
      <li class="active">
        <a href="genindex.html" title="General Index"
           accesskey="I">index</a>
      </li>
      <li class="active">
        <a href="py-modindex.html" title="Python Module Index"
           >modules</a>
      </li>
      <li class="active">
        <a href="test_models.html" title="Testing model implementations"
           accesskey="N">next</a>
      </li>
      <li class="active">
        <a href="units.html" title="Units"
           accesskey="P">previous</a>
      </li>
    </ul>
              
	    </div>
	  </div>
	</div>
        

	<div class="row-fluid">
          <div class="span9">
            
        <div class="bodywrapper">
          <div class="body" id="spc-section-body">
            
  <div class="section" id="module-base.model_base">
<span id="model-base-class"></span><h1>Model Base Class<a class="headerlink" href="#module-base.model_base" title="Permalink to this headline">¶</a></h1>
<p>This unit contains the base class for all ISM models.</p>
<div class="section" id="basics">
<h2>Basics<a class="headerlink" href="#basics" title="Permalink to this headline">¶</a></h2>
<p>The base class is as general as possible to accommodate any kind of ISM model, but it does fix a 
data model for all derived classes. The class is introspective in that it knows about its data 
content to some extent. In particular, it must be told (by the derived class constructor) which of
the following categories of data it accepts, holds, and outputs:</p>
<ul>
<li><p>inputs: the input to the model is an array of physical quantities with an arbitrary length. For 
example, in the minimal case, this can be only the density, or also temperature, metallicity 
etc. The inputs must be given as a structured numpy array. Note that individual fields in such
arrays can have dimensionality beyond just a number. For example, if the species of individual
metals were used as an input, they could be given as a vector. Inside the base class and derived
classes, the inputs are accessed as:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="n">density</span> <span class="o">=</span> <span class="n">inpt</span><span class="p">[</span><span class="s1">&#39;rho&#39;</span><span class="p">]</span>
</pre></div>
</div>
<p>which results in a numpy array of densities. A list of inputs must be set by the derived class
in the <code class="docutils literal notranslate"><span class="pre">input_names</span></code> field, and must correspond to entries in <a class="reference internal" href="common.html#base.common.input_props" title="base.common.input_props"><code class="xref py py-data docutils literal notranslate"><span class="pre">base.common.input_props</span></code></a>.</p>
</li>
<li><p>outputs: each model has a certain set of quantities that it predicts, e.g., the star formation
rate. Since these quantities depend on the nature of the model, they are entirely free, but must
be listed in the <code class="docutils literal notranslate"><span class="pre">output_names</span></code> field and must correspond to entries in 
<a class="reference internal" href="common.html#base.common.output_props" title="base.common.output_props"><code class="xref py py-data docutils literal notranslate"><span class="pre">base.common.output_props</span></code></a>. For each output quantity, there must exist a function in the 
derived class (or base class) of the same name. For example, if an effective temperature is set
as a possible output by:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="bp">self</span><span class="o">.</span><span class="n">output_names</span> <span class="o">=</span> <span class="p">[</span><span class="s1">&#39;T_eff&#39;</span><span class="p">,</span> <span class="s1">&#39;sfr&#39;</span><span class="p">]</span>
</pre></div>
</div>
<p>then there need to be <code class="docutils literal notranslate"><span class="pre">T_eff()</span></code> and <code class="docutils literal notranslate"><span class="pre">sfr()</span></code> functions that take <code class="docutils literal notranslate"><span class="pre">inpt</span></code> as a parameter:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="k">def</span> <span class="nf">T_eff</span><span class="p">(</span><span class="bp">self</span><span class="p">,</span> <span class="n">inpt</span><span class="p">):</span>
        <span class="o">...</span>
        <span class="n">T</span> <span class="o">=</span> <span class="o">...</span>
        <span class="o">...</span>
        <span class="k">return</span> <span class="n">T</span>
</pre></div>
</div>
<p>and so on. Note that the output must have the same (first) dimension as the input array, e.g.,
if the dimension of input is 20, the output must have dimension (20, ).</p>
</li>
<li><p>parameters: these are variables that can be changed and determine the physics of the model. They
are listed in <code class="docutils literal notranslate"><span class="pre">par_names</span></code> in the constructor. The user must set an initial value for each 
parameter and pass it to the constructor of the base class.</p></li>
<li><p>constants: similar to parameters, the list of model constants is set in <code class="docutils literal notranslate"><span class="pre">const_names</span></code>, but they
should not be changed after the initialization of the model. One can think of constants as 
immutable parameters that, for example, should not be changed in a fit to data. Examples include
<code class="docutils literal notranslate"><span class="pre">f_H</span></code>, the primordial hydrogen fraction. Note that the base class contains a few constants that
are common to all models.</p></li>
<li><p>internal variables: there is no hard guideline to how internal variables should be stored. The
point is that any variable (or function) that is not part of the inputs, outputs, parameters, or
constants should not be accessed from outside the model class.</p></li>
</ul>
<p>The data types are initialized in the constructor of the base class, which MUST be invoked first
after defining the data types. For example, the constructor of a derived class could read:</p>
<div class="highlight-default notranslate"><div class="highlight"><pre><span></span><span class="k">class</span> <span class="nc">MyISMModel</span><span class="p">(</span><span class="n">ISMModel</span><span class="p">):</span>

        <span class="k">def</span> <span class="nf">__init__</span><span class="p">(</span><span class="bp">self</span><span class="p">,</span> <span class="n">alpha</span> <span class="o">=</span> <span class="mf">0.1</span><span class="p">,</span> <span class="n">beta</span> <span class="o">=</span> <span class="mf">0.2</span><span class="p">,</span> <span class="n">some_fixed_setting</span> <span class="o">=</span> <span class="kc">False</span><span class="p">,</span> <span class="o">**</span><span class="n">kwargs</span><span class="p">):</span>
                                
                <span class="bp">self</span><span class="o">.</span><span class="n">input_names</span> <span class="o">=</span> <span class="p">[</span><span class="s1">&#39;rho&#39;</span><span class="p">]</span>
                <span class="bp">self</span><span class="o">.</span><span class="n">output_names</span> <span class="o">=</span> <span class="p">[</span><span class="s1">&#39;T_eff&#39;</span><span class="p">,</span> <span class="s1">&#39;sfr&#39;</span><span class="p">]</span>
                <span class="bp">self</span><span class="o">.</span><span class="n">const_names</span> <span class="o">=</span> <span class="p">[</span><span class="s1">&#39;some_fixed_setting&#39;</span><span class="p">]</span>
                <span class="bp">self</span><span class="o">.</span><span class="n">par_names</span> <span class="o">=</span> <span class="p">[</span><span class="s1">&#39;alpha&#39;</span><span class="p">,</span> <span class="s1">&#39;beta&#39;</span><span class="p">]</span>

                <span class="n">all_args</span> <span class="o">=</span> <span class="n">copy</span><span class="o">.</span><span class="n">copy</span><span class="p">(</span><span class="nb">locals</span><span class="p">())</span>
                <span class="k">del</span><span class="p">(</span><span class="n">all_args</span><span class="p">[</span><span class="s1">&#39;kwargs&#39;</span><span class="p">])</span>
                <span class="k">del</span><span class="p">(</span><span class="n">all_args</span><span class="p">[</span><span class="s1">&#39;self&#39;</span><span class="p">])</span>
                <span class="n">all_args</span><span class="o">.</span><span class="n">update</span><span class="p">(</span><span class="n">kwargs</span><span class="p">)</span>
                <span class="n">model_base</span><span class="o">.</span><span class="n">ISMModel</span><span class="o">.</span><span class="fm">__init__</span><span class="p">(</span><span class="bp">self</span><span class="p">,</span> <span class="o">**</span><span class="n">all_args</span><span class="p">)</span>
</pre></div>
</div>
<p>Here, arbitrary kwargs are also passed to the constructor of the base class, so that the user can
change it’s settings, e.g. <code class="docutils literal notranslate"><span class="pre">f_H</span></code>.</p>
</div>
<div class="section" id="module-reference">
<h2>Module reference<a class="headerlink" href="#module-reference" title="Permalink to this headline">¶</a></h2>
<dl class="class">
<dt id="base.model_base.ISMModel">
<em class="property">class </em><code class="sig-prename descclassname">base.model_base.</code><code class="sig-name descname">ISMModel</code><span class="sig-paren">(</span><em class="sig-param">f_H=0.76</em>, <em class="sig-param">gamma=1.6666667</em>, <em class="sig-param">verbose=False</em>, <em class="sig-param">**kwargs</em><span class="sig-paren">)</span><a class="headerlink" href="#base.model_base.ISMModel" title="Permalink to this definition">¶</a></dt>
<dd><p>The general ISM model base class.</p>
<p>The constructor of this class must be called by each derived class after <code class="docutils literal notranslate"><span class="pre">input_names</span></code>, 
<code class="docutils literal notranslate"><span class="pre">output_names</span></code>, <code class="docutils literal notranslate"><span class="pre">par_names</span></code>, and <code class="docutils literal notranslate"><span class="pre">const_names</span></code> have been set.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><dl class="simple">
<dt><strong>f_H: float</strong></dt><dd><p>The primordial hydrogen fraction.</p>
</dd>
<dt><strong>gamma: float</strong></dt><dd><p>The adiabatic index assumed for the ISM gas.</p>
</dd>
<dt><strong>verbose: bool</strong></dt><dd><p>Whether the model should output log statements.</p>
</dd>
<dt><strong>kwargs</strong></dt><dd><p>All arguments that are listed as parameters or constants in the derived base class.</p>
</dd>
</dl>
</dd>
</dl>
<p class="rubric">Methods</p>
<table class="longtable docutils align-default">
<colgroup>
<col style="width: 10%" />
<col style="width: 90%" />
</colgroup>
<tbody>
<tr class="row-odd"><td><p><a class="reference internal" href="#base.model_base.ISMModel.T_from_u" title="base.model_base.ISMModel.T_from_u"><code class="xref py py-obj docutils literal notranslate"><span class="pre">T_from_u</span></code></a>(self, u[, ionized])</p></td>
<td><p>Temperature given an internal energy.</p></td>
</tr>
<tr class="row-even"><td><p><a class="reference internal" href="#base.model_base.ISMModel.coolingRate" title="base.model_base.ISMModel.coolingRate"><code class="xref py py-obj docutils literal notranslate"><span class="pre">coolingRate</span></code></a>(self, rho, T)</p></td>
<td><p>Cooling rate divided by nH^2.</p></td>
</tr>
<tr class="row-odd"><td><p><a class="reference internal" href="#base.model_base.ISMModel.dln_Peff_dln_rho" title="base.model_base.ISMModel.dln_Peff_dln_rho"><code class="xref py py-obj docutils literal notranslate"><span class="pre">dln_Peff_dln_rho</span></code></a>(self, inpt)</p></td>
<td><p>Logarithmic slope of the equation of state.</p></td>
</tr>
<tr class="row-even"><td><p><a class="reference internal" href="#base.model_base.ISMModel.jeansLength" title="base.model_base.ISMModel.jeansLength"><code class="xref py py-obj docutils literal notranslate"><span class="pre">jeansLength</span></code></a>(self, rho, u)</p></td>
<td><p>Jeans length of the gas.</p></td>
</tr>
<tr class="row-odd"><td><p><a class="reference internal" href="#base.model_base.ISMModel.meanParticleWeight" title="base.model_base.ISMModel.meanParticleWeight"><code class="xref py py-obj docutils literal notranslate"><span class="pre">meanParticleWeight</span></code></a>(self[, ionized])</p></td>
<td><p>The mean weight of a gas particle in gram.</p></td>
</tr>
<tr class="row-even"><td><p><a class="reference internal" href="#base.model_base.ISMModel.pressure" title="base.model_base.ISMModel.pressure"><code class="xref py py-obj docutils literal notranslate"><span class="pre">pressure</span></code></a>(self, rho, u)</p></td>
<td><p>Logarithmic slope of the equation of state.</p></td>
</tr>
<tr class="row-odd"><td><p><a class="reference internal" href="#base.model_base.ISMModel.sfr" title="base.model_base.ISMModel.sfr"><code class="xref py py-obj docutils literal notranslate"><span class="pre">sfr</span></code></a>(self, inpt, \*\*kwargs)</p></td>
<td><p>SFR in astrophysical units.</p></td>
</tr>
<tr class="row-even"><td><p><a class="reference internal" href="#base.model_base.ISMModel.u_from_T" title="base.model_base.ISMModel.u_from_T"><code class="xref py py-obj docutils literal notranslate"><span class="pre">u_from_T</span></code></a>(self, T[, ionized])</p></td>
<td><p>Energy per unit mass in erg/g given a temperature.</p></td>
</tr>
</tbody>
</table>
<dl class="method">
<dt id="base.model_base.ISMModel.meanParticleWeight">
<code class="sig-name descname">meanParticleWeight</code><span class="sig-paren">(</span><em class="sig-param">self</em>, <em class="sig-param">ionized=True</em><span class="sig-paren">)</span><a class="headerlink" href="#base.model_base.ISMModel.meanParticleWeight" title="Permalink to this definition">¶</a></dt>
<dd><p>The mean weight of a gas particle in gram.</p>
<p>If the material is non-ionized, we get 1 particle for each H atom (weight fH) and 1 
particle for each He atom (weight (1-fH)/4). If the material is completely ionized, we get 
2 particles for each H atom and 3 for each He atom.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><dl class="simple">
<dt><strong>ionized: bool</strong></dt><dd><p>Whether the gas is entirely ionized or entirely neutral.</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="base.model_base.ISMModel.u_from_T">
<code class="sig-name descname">u_from_T</code><span class="sig-paren">(</span><em class="sig-param">self</em>, <em class="sig-param">T</em>, <em class="sig-param">ionized=True</em><span class="sig-paren">)</span><a class="headerlink" href="#base.model_base.ISMModel.u_from_T" title="Permalink to this definition">¶</a></dt>
<dd><p>Energy per unit mass in erg/g given a temperature.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><dl class="simple">
<dt><strong>T: array_like</strong></dt><dd><p>Temperature in Kelvin.</p>
</dd>
<dt><strong>ionized: bool</strong></dt><dd><p>Whether the gas is entirely ionized or entirely neutral.</p>
</dd>
</dl>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><dl class="simple">
<dt>u: array_like</dt><dd><p>Internal energy in (cm/s)^2.</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="base.model_base.ISMModel.T_from_u">
<code class="sig-name descname">T_from_u</code><span class="sig-paren">(</span><em class="sig-param">self</em>, <em class="sig-param">u</em>, <em class="sig-param">ionized=True</em><span class="sig-paren">)</span><a class="headerlink" href="#base.model_base.ISMModel.T_from_u" title="Permalink to this definition">¶</a></dt>
<dd><p>Temperature given an internal energy.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><dl class="simple">
<dt><strong>u: array_like</strong></dt><dd><p>Internal energy in (cm/s)^2.</p>
</dd>
<dt><strong>ionized: bool</strong></dt><dd><p>Whether the gas is entirely ionized or entirely neutral.</p>
</dd>
</dl>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><dl class="simple">
<dt>T: array_like</dt><dd><p>Temperature in Kelvin.</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="base.model_base.ISMModel.coolingRate">
<code class="sig-name descname">coolingRate</code><span class="sig-paren">(</span><em class="sig-param">self</em>, <em class="sig-param">rho</em>, <em class="sig-param">T</em><span class="sig-paren">)</span><a class="headerlink" href="#base.model_base.ISMModel.coolingRate" title="Permalink to this definition">¶</a></dt>
<dd><p>Cooling rate divided by nH^2.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><dl class="simple">
<dt><strong>rho: array_like</strong></dt><dd><p>Density.</p>
</dd>
<dt><strong>T: array_like</strong></dt><dd><p>Temperature in Kelvin.</p>
</dd>
</dl>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><dl class="simple">
<dt>Lambda/nH^2: array_like</dt><dd><p>Cooling rate in units of erg/s cm^3.</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="base.model_base.ISMModel.pressure">
<code class="sig-name descname">pressure</code><span class="sig-paren">(</span><em class="sig-param">self</em>, <em class="sig-param">rho</em>, <em class="sig-param">u</em><span class="sig-paren">)</span><a class="headerlink" href="#base.model_base.ISMModel.pressure" title="Permalink to this definition">¶</a></dt>
<dd><p>Logarithmic slope of the equation of state.</p>
<p>This function does not work if the input has fewer than 2 array points, as no gradient can
be computed in that case.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><dl class="simple">
<dt><strong>rho: array_like</strong></dt><dd><p>Density.</p>
</dd>
<dt><strong>u: array_like</strong></dt><dd><p>Internal energy in (cm/s)^2.</p>
</dd>
</dl>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><dl class="simple">
<dt>pressure: array_like</dt><dd><p>The gas pressure.</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="base.model_base.ISMModel.jeansLength">
<code class="sig-name descname">jeansLength</code><span class="sig-paren">(</span><em class="sig-param">self</em>, <em class="sig-param">rho</em>, <em class="sig-param">u</em><span class="sig-paren">)</span><a class="headerlink" href="#base.model_base.ISMModel.jeansLength" title="Permalink to this definition">¶</a></dt>
<dd><p>Jeans length of the gas.</p>
<p>The Jeans length can be used as a rough estimate for the size of a self-gravitating
system.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><dl class="simple">
<dt><strong>rho: array_like</strong></dt><dd><p>Density.</p>
</dd>
<dt><strong>u: array_like</strong></dt><dd><p>Internal energy in (cm/s)^2.</p>
</dd>
</dl>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><dl class="simple">
<dt>slope: array_like</dt><dd><p>The Jeans length in cm.</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="base.model_base.ISMModel.sfr">
<code class="sig-name descname">sfr</code><span class="sig-paren">(</span><em class="sig-param">self</em>, <em class="sig-param">inpt</em>, <em class="sig-param">**kwargs</em><span class="sig-paren">)</span><a class="headerlink" href="#base.model_base.ISMModel.sfr" title="Permalink to this definition">¶</a></dt>
<dd><p>SFR in astrophysical units.</p>
<p>Internally, the <code class="docutils literal notranslate"><span class="pre">sfr_cgs</span></code> function of derived subclasses should compute the SFR in 
g/s/cm^3. Since these units are not particularly intuitive, this function converts them.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><dl class="simple">
<dt><strong>inpt: array_like</strong></dt><dd><p>Structured array with input parameters.</p>
</dd>
<dt><strong>kwargs</strong></dt><dd><p>Arguments to be passed to the internal sfr function.</p>
</dd>
</dl>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><dl class="simple">
<dt>sfr: array_like</dt><dd><p>SFR in Msun / yr / kpc^3.</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

<dl class="method">
<dt id="base.model_base.ISMModel.dln_Peff_dln_rho">
<code class="sig-name descname">dln_Peff_dln_rho</code><span class="sig-paren">(</span><em class="sig-param">self</em>, <em class="sig-param">inpt</em><span class="sig-paren">)</span><a class="headerlink" href="#base.model_base.ISMModel.dln_Peff_dln_rho" title="Permalink to this definition">¶</a></dt>
<dd><p>Logarithmic slope of the equation of state.</p>
<p>This function does not work if the input has fewer than 2 array points, as no gradient can
be computed in that case.</p>
<dl class="field-list simple">
<dt class="field-odd">Parameters</dt>
<dd class="field-odd"><dl class="simple">
<dt><strong>inpt: array_like</strong></dt><dd><p>Structured array with input parameters.</p>
</dd>
</dl>
</dd>
<dt class="field-even">Returns</dt>
<dd class="field-even"><dl class="simple">
<dt>slope: array_like</dt><dd><p>The logarithmic slope of the equation of state.</p>
</dd>
</dl>
</dd>
</dl>
</dd></dl>

</dd></dl>

</div>
</div>


          </div>
        </div>
          </div>
      <div class="spc-rightsidebar span3">
        <div class="sphinxsidebarwrapper">
  <h3><a href="index.html">Table of Contents</a></h3>
  <ul>
<li><a class="reference internal" href="#">Model Base Class</a><ul>
<li><a class="reference internal" href="#basics">Basics</a></li>
<li><a class="reference internal" href="#module-reference">Module reference</a></li>
</ul>
</li>
</ul>

  <h4>Previous topic</h4>
  <p class="topless"><a href="units.html"
                        title="previous chapter">Units</a></p>
  <h4>Next topic</h4>
  <p class="topless"><a href="test_models.html"
                        title="next chapter">Testing model implementations</a></p>
        </div>
      </div>
        </div>
      </div>
    </div>

    <div class="container container-navbar-bottom">
      <div class="spc-navbar">
        
      </div>
    </div>
    <div class="container">
    <div class="footer">
    <div class="row-fluid">
    <ul class="inline pull-left">
      <li>
        &copy; Copyright 2015-2019, Benedikt Diemer.
      </li>
      <li>
      Created using <a href="http://sphinx.pocoo.org/">Sphinx</a> 2.2.0.
      </li>
    </ul>
    </div>
    </div>
    </div>
  </body>
</html>